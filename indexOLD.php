<?php
	$pageTitle = "Snowbird - Car Shipping";
	$pageKeywords = "sterling virginia auto repair, sterling virginia auto shop, sterling virginia custom auto shop, sterling virginial restoration, sterling virginia engine repair, sterling virginia auto repair shop";
	$pageDesc = "Revtech Performance, A full service customs, restoration, and auto repair shop located in beautiful Sterling Virginia.";
	require_once("tehPHP/snowBirdHeader.php");
?>


<?php
	//require_once("./tehPHP/mainRevolutionSlider.php");
?>

<link href="https://fonts.googleapis.com/css?family=Just+Another+Hand" rel="stylesheet">

<div class="snowBirdBGDiv snowBirdIndexBG">
	<div class="snowBirdBGDivTitle">
		Best Price Guantee.
	</div>
	<div class="snowBirdBGDivSubTitle">
		Ship your car Anywhere
	</div>
</div>

<style>
	.snowBirdContentWrapper{max-width: 1100px; margin: auto;}
	.snowBirdBGDivTitle{font-size: 3em; color: #FFFFFF; text-transform: uppercase;font-weight: bold;text-shadow: 2px 2px #343434;}
	.snowBirdBGDivSubTitle{color: #FFFFFF; font-size: 1.3em; text-shadow: 2px 2px #343434;}
	.snowBirdContentTitle{font-family: 'Roboto'; font-size: 1.5em;  font-weight: 500;}
	.snowBirdContentSubTitle{color: #898989; margin-top: -5px;}
	.snowBirdStateShippingList{padding: 0; list-style: none;}
	.snowBirdContentTitleShell{border-left: 2px solid #27B4E1; margin: 40px 10px; padding: 5px 20px;}
	.snowBirdContentColumnPaddingShell{padding: 10px 20px;}
	.usMap{max-width: 700px; margin: auto;}

	.contactUsFormShell{background: #FFFFFF; padding: 40px 40px 20px 40px; border: 1px solid #EFEFEF; box-shadow: 3px 3px 5px #CDCDCD; max-width: 463px; margin: auto;}
	.contactUsFormTitle{text-align: center; color: #18A3C4; padding-bottom: 20px; font-weight: bold;}
	.contactUsFormTitleBlack{text-align: left; color: #686868; font-weight: bold; padding-top: 10px;}
	.contactUsFormInput{width: 99%; display: block;  box-sizing: border-box; padding: 10px !important;}
	.contactTextArea{width: 100%; box-sizing: border-box; margin: 8px 0; height: 150px;}
	input,select.contactUsFormInput,.contactTextArea{border: 1px solid #FFFFFF;outline: none;padding: 10px;color: #989898;font-size: .8em;background: #EFEFEF;-webkit-border-radius: 3px;-khtml-border-radius: 3px;border-radius: 3px;}
	.kulaGrowButtonShell a{text-decoration: none; transition: all .3s ease;  padding: 15px 40px; margin: auto; text-align: center; display: table-cell; font-size: .8em; color: #FFFFFF !important; font-weight: bold;}
	.kulaGrowButtonShell a:hover{padding: 15px 80px; color: #000000 !important;}
	.growButtonRedefine{background: #18A3C4; color: #FFFFFF; !important;}
	.growButtonRedefine:hover{background: #ADE4F1; cursor: pointer; color: #000000;}
</style>


<div class="snowBirdContentWrapper">
	
	<div class="row">
		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
			<a class="snowBird" href="#">
			</a>
		</div>
	</div>
	<div class="contactUsFormShell" style="margin-top: -200px; position: relative; z-index:56;">
		<form id="contactUsForm" class="form-horizontal" role="form" method="POST" action="<?php echo $tehAbsoluteURL; ?>tehPHP/validations/contactUsFormValidation.php">
			<div class="contactUsFormTitleBlack">
				Originating City - Destination
			</div>
			<div class="row">
				<div class="col-lg-6 col-md-7 col-sm-12 col-xs-12">
					<div>
						<input type="name" class="contactUsFormInput validate[required] text-input" name="contactNameField" id="contactNameField" placeholder="Originating City">
					</div>
				</div>
				<div class="col-lg-2 col-md-1 col-sm-12 col-xs-12">
					<div>
						<input size="2" maxlength="2" type="name" class="contactUsFormInput validate[required] text-input" name="contactNameField" id="contactNameField" placeholder="ST">
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<div>
						<input size="6" maxlength="6" type="name" class="contactUsFormInput validate[required] text-input" name="contactNameField" id="contactNameField" placeholder="Zip">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6 col-md-7 col-sm-12 col-xs-12">
					<div>
						<input type="name" class="contactUsFormInput validate[required] text-input" name="contactNameField" id="contactNameField" placeholder="Destination City">
					</div>
				</div>
				<div class="col-lg-2 col-md-1 col-sm-12 col-xs-12">
					<div>
						<input size="2" maxlength="2" type="name" class="contactUsFormInput validate[required] text-input" name="contactNameField" id="contactNameField" placeholder="ST">
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<div>
						<input size="6" maxlength="6" type="name" class="contactUsFormInput validate[required] text-input" name="contactNameField" id="contactNameField" placeholder="Zip">
					</div>
				</div>
			</div>
			<div class="contactUsFormTitleBlack">
				Desired Pick Up / Delivery Date
			</div>
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div>
						<input type="name" class="contactUsFormInput validate[required] text-input" name="contactNameField" id="contactNameField" placeholder="Pickup Date">
					</div>
				</div><div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div>
						<input type="name" class="contactUsFormInput validate[required] text-input" name="contactNameField" id="contactNameField" placeholder="Delivery Date">
					</div>
				</div>
			</div>
			<div class="contactUsFormTitleBlack">
				Contact Info
			</div>
			<div>
				<input type="name" class="contactUsFormInput validate[required] text-input" name="contactNameField" id="contactNameField" placeholder="Name">
			</div>
			<div>
				<input type="email" class="contactUsFormInput validate[required,custom[email]] text-input" name="contactEmailField" id="contactEmailField" placeholder="Email">
			</div>
			<div>
				<input type="phone" class="contactUsFormInput validate[required,custom[phone]] text-input" name="contactPhoneField" id="contactPhoneField" placeholder="Phone">
			</div>
			<br />
			<div class="contactUsFormTitle">
				FREE Quote - No Obligation
			</div>
			<center>
				<button type="submit" class="kulaGrowButtonShell" style="background: none; border: none;">
					<a class="growButtonRedefine">
						Request Quote
					</a>
				</button>
			</center>
		</form>
	</div>
	<div class="snowBirdContentTitleShell">
		<div class="snowBirdContentTitle">
			Serving 48 Contiguous States
		</div>
		<div class="snowBirdContentSubTitle">
			Voted #1 among SnowBirds
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="snowBirdContentColumnPaddingShell usMap">
				<center>
					<img src="./layout/images/usMap.png" width="100%" />
				</center>
			</div>
		</div>
	</div>
	<div class="row" style="padding: 20px 40px;">
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<ul class="snowBirdStateShippingList">
				<li>
					<a href="#">
						Alabama Car Shipping
					</a>
				</li>
				<li>
					<a href="#">
						Alaska Car Shipping
					</a>
				</li>
				<li>
					<a href="#">
						Arizona Car Transport
					</a>
				</li>
				<li>
					<a href="#">
						Arkansas Car Shipping
					</a>
				</li>
			</ul>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<ul class="snowBirdStateShippingList">
				<li>
					<a href="#">
						Utah Car Shipping
					</a>
				</li>
				<li>
					<a href="#">
						Virginia Car Shipping
					</a>
				</li>
				<li>
					<a href="#">
						Washington Car Transport
					</a>
				</li>
				<li>
					<a href="#">
						Washington DC Car Shipping
					</a>
				</li>
			</ul>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<ul class="snowBirdStateShippingList">
				<li>
					<a href="#">
						Alabama Car Shipping
					</a>
				</li>
				<li>
					<a href="#">
						Alaska Car Shipping
					</a>
				</li>
				<li>
					<a href="#">
						Arizona Car Transport
					</a>
				</li>
				<li>
					<a href="#">
						Arkansas Car Shipping
					</a>
				</li>
			</ul>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<ul class="snowBirdStateShippingList">
				<li>
					<a href="#">
						Utah Car Shipping
					</a>
				</li>
				<li>
					<a href="#">
						Virginia Car Shipping
					</a>
				</li>
				<li>
					<a href="#">
						Washington Car Transport
					</a>
				</li>
				<li>
					<a href="#">
						Washington DC Car Shipping
					</a>
				</li>
			</ul>
		</div>
		<div class="snowBirdContentTitle">
		Recent Transports
	</div>
	<div class="snowBirdContentTitle">
		Services Icons
	</div>
	</div>
</div>

<?php
	// if (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1")
	// {
	// 	require_once("./tehPHP/revTechFooter.php");
	// }
	// else
	// {
	// 	require_once("../tehPHP/revTechFooter.php");
	// }
?>
