<?php
	$pageTitle = "Snowbird Hauling - Cross Country Auto Transport";
	$pageKeywords = "cross country auto transport,transport auto across country,cross country autos transport,transport autos across country,cross country automobile transport,transport automobile across country,cross country automobiles transport,transport automobiles across country,cross country truck transport,transport truck across country,cross country trucks transport,transport trucks across country,cross country vehicle transport,transport vehicle across country,cross country vehicles transport,transport vehicles across country,cross country auto transporting,transporting auto across country,cross country autos transporting,transporting autos across country,cross country automobile transporting,transporting automobile across country,cross country automobiles transporting,transporting automobiles across country,cross country truck transporting,transporting truck across country,cross country trucks transporting,transporting trucks across country,cross country vehicle transporting,transporting vehicle across country,cross country vehicles transporting,transporting vehicles across country,cross country auto haul,haul auto across country,cross country autos haul,haul autos across country,cross country automobile haul,haul automobile across country,cross country automobiles haul,haul automobiles across country,cross country truck haul,haul truck across country,cross country trucks haul,haul trucks across country,cross country vehicle haul,haul vehicle across country,cross country vehicles haul,haul vehicles across country,cross country auto hauling,hauling auto across country,cross country autos hauling,hauling autos across country,cross country automobile hauling,hauling automobile across country,cross country automobiles hauling,hauling automobiles across country,cross country truck hauling,hauling truck across country,cross country trucks hauling,hauling trucks across country,cross country vehicle hauling,hauling vehicle across country,cross country vehicles hauling,hauling vehicles across country,cross country auto ship,ship auto across country,cross country autos ship,ship autos across country,cross country automobile ship,ship automobile across country,cross country automobiles ship,ship automobiles across country,cross country truck ship,ship truck across country,cross country trucks ship,ship trucks across country,cross country vehicle ship,ship vehicle across country,cross country vehicles ship,ship vehicles across country,cross country auto shipping,shipping auto across country,cross country autos shipping,shipping autos across country,cross country automobile shipping,shipping automobile across country,cross country automobiles shipping,shipping automobiles across country,cross country truck shipping,shipping truck across country,cross country trucks shipping,shipping trucks across country,cross country vehicle shipping,shipping vehicle across country,cross country vehicles shipping,shipping vehicles across country";
	$pageDesc = "Snowbord hauling - Specializing in cross country Auto Transporting. We take away the hassle from transporting your car across the states.";
	require_once("../tehPHP/snowBirdHeader.php");
?>

<div class="snowbirdBG whiteText" style="background: url(<?php echo $tehAbsoluteURL; ?>layout/backgrounds/nyToFloridaCarrier.jpg) no-repeat top center;">
	<div class="centerWrap whiteBG stdBoxShadowOnColorBG contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentFormShell stdBoxShadow">
					<?php
						require_once("../tehPHP/tehRefferalForm.php")
					?>
				</div>
				<div class="contentTitle bold">
					Cross Country Auto Transportation
				</div>
				<div class="contentSubTitle grayText">
					Let Snowbird transport your car or truck across the country.
				</div>
				<br />
				<p class="grayText contentParagraph">
					You have two choices when moving your vehicle across the country. You may opt to make the drive yourself, or hire a service to ship haul it for you. While a road trip across this beautiful country is somethign everybody should do at least once. Once is generally enough, and if you find yourself in a situation where you're needing your car delivered half a country away. Look no further than Snowbird Hauling.
				</p>
				<p class="grayText contentParagraph">
					The process for hiring a car shipping service is simple. Just fill out our no obligation quote form and we'll get pricing quotes from thousands of car hauling service providers in your area. Once you've selected a price and company, we'll arrange the whole thing including pick up and delivery directions.
				</p>
				<p class="grayText contentParagraph">
					At Snowbird, it's our job to match customers like you with independent and affiliated car haulers capable of delivering your precious vehicle safely from state to state.
				</p>
				<div class="contentMiniTitle bold">
					Benefits of Hiring a Cross Country Auto Transport Service:
				</div>
				<br />
				<ul class="contentList">
					<li>
						No-Risk, Licensed and Insured
					</li>
					<li>
						Avoid the mileage on your car. Especially, helpful if it's a lease.
					</li>
					<li>
						Load up your car with your stuff, like a trailer.
					</li>
					<li>
						Save Time, Save Money
					</li>
				</ul>
				<!--
				<img class="contentIMG" width="300" title="Car Transport NY to FL" alt="NY, FL Sign" src="<?php echo $tehAbsoluteURL; ?>layout/images/NYtoFL.jpg" />
				-->
			</div>
		</div>
	</div>
</div>



<?php
	//if (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1")
	if( (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1") || (dirname($_SERVER['PHP_SELF']) == "/"))
	{
		require_once("./tehPHP/snowBirdFooter.php");
	}
	else
	{
		require_once("../tehPHP/snowBirdFooter.php");
	}
?>