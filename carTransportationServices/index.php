<?php
	$pageTitle = "Snowbird Hauling - Car Transportation Service";
	$pageKeywords = "car transportation service";
	$pageDesc = "Snowbord hauling describing the options and benefits to hiring a car transportation service. And Complete our form for a Free Quote!";
	require_once("../tehPHP/snowBirdHeader.php");
?>

<div class="snowbirdBG whiteText" style="background: url(<?php echo $tehAbsoluteURL; ?>layout/backgrounds/nyToFloridaCarrier.jpg) no-repeat top center;">
	<div class="centerWrap whiteBG stdBoxShadowOnColorBG contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentFormShell stdBoxShadow">
					<?php
						require_once("../tehPHP/tehRefferalForm.php")
					?>
				</div>
				<div class="contentTitle bold">
					Florida Car Transportation Services
				</div>
				<p class="grayText contentParagraph">
					If you're needing your vehicle to be available to you at your home away from home, you really only have two options. Either drive it yourself or hire a car transportation service.  Hiring a driver has never been easier and when you consider the high costs of gas, your time spent driving, and the wear and tear on your vehicle the cost is well worth it.
				</p>
				<p class="grayText contentParagraph">
					Snowbird Hauling offers two types of car shipping options - open transport or enclosed transport.
				</p>
				<div class="contentMiniTitle bold">
					Open Transport
				</div>
				<p class="grayText contentParagraph">
					Open transport is the standard. Cars are stacked and chained to these multilevel platform trailers. While these trailers leave your car exposed to the elements they are safely strapped down and all precautions are taken to ensure your vehicle arrives safely at the destination.
				</p>
				<div class="contentMiniTitle bold">
					Enclosed Transport
				</div>
				<p class="grayText contentParagraph">
					Enclosed transporting offers an even safer option for your vehicles. The cost is going to run about 30-50% more expensive than the standard open transport as space is much more limited.  Enclosed transporting protects your car from most elements. 
				</p>
				<div class="contentMiniTitle bold">
					The benefits of hiring an auto transport service:
				</div>
				<br />
				<ul class="contentList">
					<li>
						No-Risk, Licensed and Insured
					</li>
					<li>
						Avoid the mileage on your car. Especially, helpful if it's a lease.
					</li>
					<li>
						Save Time, Save Money
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>



<?php
	//if (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1")
	if( (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1") || (dirname($_SERVER['PHP_SELF']) == "/"))
	{
		require_once("./tehPHP/snowBirdFooter.php");
	}
	else
	{
		require_once("../tehPHP/snowBirdFooter.php");
	}
?>


