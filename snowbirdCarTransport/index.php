<?php
	$pageTitle = "Snowbird Hauling - Snowbird Car Transport Services";
	$pageKeywords = "snowbird car transport,snowbird auto transport,snowbird automobile transport,snowbird vehicle transport,snowbird truck transport,snow bird car transport,snow bird auto transport,snow bird automobile transport,snow bird vehicle transport,snow bird truck transport,snowbird car transporting,snowbird auto transporting,snowbird automobile transporting,snowbird vehicle transporting,snowbird truck transporting,snow bird car transporting,snow bird auto transporting,snow bird automobile transporting,snow bird vehicle transporting,snow bird truck transporting,snowbird car ship,snowbird auto ship,snowbird automobile ship,snowbird vehicle ship,snowbird truck ship,snow bird car ship,snow bird auto ship,snow bird automobile ship,snow bird vehicle ship,snow bird truck ship,snowbird car shpping,snowbird auto shpping,snowbird automobile shpping,snowbird vehicle shpping,snowbird truck shpping,snow bird car shpping,snow bird auto shpping,snow bird automobile shpping,snow bird vehicle shpping,snow bird truck shpping,snowbird car haul,snowbird auto haul,snowbird automobile haul,snowbird vehicle haul,snowbird truck haul,snow bird car haul,snow bird auto haul,snow bird automobile haul,snow bird vehicle haul,snow bird truck haul,snowbird car hauling,snowbird auto hauling,snowbird automobile hauling,snowbird vehicle hauling,snowbird truck hauling,snow bird car hauling,snow bird auto hauling,snow bird automobile hauling,snow bird vehicle hauling,snow bird truck hauling";
	$pageDesc = "Snowbord hauling - Specializing in Snowbird Auto Transporting. We take away the hassle from transporting your while you Snowbird!";
	require_once("../tehPHP/snowBirdHeader.php");
?>

<div class="snowbirdBG whiteText" style="background: url(<?php echo $tehAbsoluteURL; ?>layout/backgrounds/nyToFloridaCarrier.jpg) no-repeat top center;">
	<div class="centerWrap whiteBG stdBoxShadowOnColorBG contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentFormShell stdBoxShadow">
					<?php
						require_once("../tehPHP/tehRefferalForm.php")
					?>
				</div>
				<div class="contentTitle bold">
					Snowbird Car Transport
				</div>
				<div class="contentSubTitle grayText">
					Let Snowbird Hauling do the driving.
				</div>
				<br />
				<p class="grayText contentParagraph">
					Snowbirds have been migrating south in the winter to Florida and North in the summertime to New York, DC, and Canada for generations.  From October, to May, the sunshine state welcomes seasoned Snowbirds with open arms as beaches, restaurants, and pubs fill with migratory tourists called Snowbirds.
				</p>
				<p class="grayText contentParagraph">
					Snowbird Hauling offers year-round car transportation services. We'll pick up your car at your door and deliver it to where ever is required with no-risk, no worry, and no hassle.
				</p>
				<div class="contentPropaganda snowBirdBlue">
					No Hassle - Snowbird Car Transportation Services
				</div>
				<p class="grayText contentParagraph">
					With just 3 easy steps. You'll be on your way to your destination to meet your vehicle.
				</p>
				<ul class="contentListSteps">
					<li>
						<a class="contentListCircleShell">
							<span>1</span> Complete our Free Quote form
						</a>
					</li>
					<li>
						<a class="contentListCircleShell">
							<span>2</span> We pick up your vehicle.
						</a>
					</li>
					<li>
						<a class="contentListCircleShell">
							<span>3</span> We deliver it to your door.
						</a>
					</li>
				</ul>
				<br /><br /><br /><br /><br /><br /><br />
				<div class="contentMiniTitle bold">
					Types of Snowbird Car Transportation
				</div>
				<div class="contentSubTitle grayText">
					You've got options.
				</div>
				<br /><br />
				<div class="gridShellThirtyThree" style="grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));">
					<div class="gridColumnShell">
						<div class="contentSubTitle bold">
							Open Snowbird Car Transport
						</div>
						<p class="grayText contentParagraph">
							Open transport is fast, efficient, and reliable. While damage to your vehicle is possible, Snowbird hauling only employs licensed and insured drivers so you may rest easily knowing we've taken every precaution available to ensure your vehicle is delivered unharmed.
						</p>
					</div>
					<div class="gridColumnShell">
						<div class="contentSubTitle bold">
							Enclosed Snowbird Car Transport
						</div>
						<p class="grayText contentParagraph">
							Enclosed transport is available and will generally run about 35% more expensive than standard open air vehicle transportation. Just let us know that you'd prefer an enclosure and we'll find you the driver.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<?php
	//if (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1")
	if( (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1") || (dirname($_SERVER['PHP_SELF']) == "/"))
	{
		require_once("./tehPHP/snowBirdFooter.php");
	}
	else
	{
		require_once("../tehPHP/snowBirdFooter.php");
	}
?>