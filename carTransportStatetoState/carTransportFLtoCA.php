<?php
	$pageTitle = "Snowbird Hauling - Transport Car from Florida to California";
	$pageKeywords = "transport car from florida to california,transport auto from florida to california,transport automobile from florida to california,transport vehicle from florida to california,transport truck from florida to california,transporting car from florida to california,transporting auto from florida to california,transporting automobile from florida to california,transporting vehicle from florida to california,transporting truck from florida to california,haul car from florida to california,haul auto from florida to california,haul automobile from florida to california,haul vehicle from florida to california,haul truck from florida to california,hauling car from florida to california,hauling auto from florida to california,hauling automobile from florida to california,hauling vehicle from florida to california,hauling truck from florida to california,ship car from florida to california,ship auto from florida to california,ship automobile from florida to california,ship vehicle from florida to california,ship truck from florida to california,shipping car from florida to california,shipping auto from florida to california,shipping automobile from florida to california,shipping vehicle from florida to california,shipping truck from florida to california,transport car from FL to CA,transport auto from FL to CA,transport automobile from FL to CA,transport vehicle from FL to CA,transport truck from FL to CA,transporting car from FL to CA,transporting auto from FL to CA,transporting automobile from FL to CA,transporting vehicle from FL to CA,transporting truck from FL to CA,haul car from FL to CA,haul auto from FL to CA,haul automobile from FL to CA,haul vehicle from FL to CA,haul truck from FL to CA,hauling car from FL to CA,hauling auto from FL to CA,hauling automobile from FL to CA,hauling vehicle from FL to CA,hauling truck from FL to CA,ship car from FL to CA,ship auto from FL to CA,ship automobile from FL to CA,ship vehicle from FL to CA,ship truck from FL to CA,shipping car from FL to CA,shipping auto from FL to CA,shipping automobile from FL to CA,shipping vehicle from FL to CA,shipping truck from FL to CA";
	$pageDesc = "Snowbord hauling - If you need to transport your car from Florida to California, look no further!";
	require_once("../tehPHP/snowBirdHeader.php");
?>

<div class="snowbirdBG whiteText" style="background: url(<?php echo $tehAbsoluteURL; ?>layout/backgrounds/nyToFloridaCarrier.jpg) no-repeat top center;">
	<div class="centerWrap whiteBG stdBoxShadowOnColorBG contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentFormShell stdBoxShadow">
					<?php
						require_once("../tehPHP/tehRefferalForm.php")
					?>
				</div>
				<div class="contentTitle bold">
					Transport Car from Florida to California
				</div>
				<div class="contentSubTitle grayText">
					From the Sunshine state to the Golden State
				</div>
				<br />
				<p class="grayText contentParagraph">
					Transporting your vehicle from Florida to California means a 2,700 mile drive across the country. This could mean a 40+ hour endeavor if you plan on making the drive yourself. Or. You could hire a car transportation company and they will pick up your car and deliver it to wherever is needed.
				</p>
				<p class="grayText contentParagraph">
					Snowbird Hauling offers car transportation services from Florida to California. We'll pick up and deliver your car with no risk, no worry as all of our drivers are licensed and insured.
				</p>
				<div class="contentPropaganda snowBirdBlue">
					Transport your car from Florida to California! - No Risk
				</div>
				<p class="grayText contentParagraph">
					With just 3 easy steps. You'll be on your way to your destination to meet your vehicle.
				</p>
				<ul class="contentListSteps">
					<li>
						<a class="contentListCircleShell">
							<span>1</span> Complete our Free Quote form
						</a>
					</li>
					<li>
						<a class="contentListCircleShell">
							<span>2</span> We pick up your vehicle.
						</a>
					</li>
					<li>
						<a class="contentListCircleShell">
							<span>3</span> We deliver it to your door.
						</a>
					</li>
				</ul>
				<br /><br /><br /><br /><br /><br /><br />
				<div class="contentMiniTitle bold">
					Cost to transport from Florida to California
				</div>
				<div class="contentSubTitle grayText">
					Prices vary according to season, demand, and availability.
				</div>
				<p class="grayText contentParagraph">
					Pricing  varies according to availability and demand. There are literally thousands of car carriers with vacant spots probably traveling back and forth between Florida and California. At Snowbird, we try to contact those drivers and fill up their carriers so that our customers enjoy a reduce cost car transport.
				</p>
				<div class="row snowBirdDisplayTextSize snowBirdBlue">
					<div class="col-lg-6 bold col-md-6">
						Route
					</div>
					<div class="col-lg-2 bold col-md-2">
						Car
					</div>
					<div class="col-lg-2 bold col-md-2">
						SUV
					</div>
					<div class="col-lg-2 bold col-md-2">
						Exotic
					</div>
				</div>
				<div class="row snowBirdDisplayTextSize snowBirdBlue">
					<div class="col-lg-6 col-md-6">
						Los Angeles <i class="fa fa-arrow-right" aria-hidden="true"></i> Boca Raton 
					</div>
					<div class="col-lg-2 col-md-2">
						$749
					</div>
					<div class="col-lg-2 col-md-2">
						$999
					</div>
					<div class="col-lg-2 col-md-2">
						$1500+
					</div>
					<div class="col-lg-6 col-md-6">
						San Francisco <i class="fa fa-arrow-right" aria-hidden="true"></i> West Palm Beach 
					</div>
					<div class="col-lg-2 col-md-2">
						$869
					</div>
					<div class="col-lg-2 col-md-2">
						$1049
					</div>
					<div class="col-lg-2 col-md-2">
						$1700+
					</div>
				</div>
				<br /><br />
			</div>
		</div>
	</div>
</div>



<?php
	//if (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1")
	if( (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1") || (dirname($_SERVER['PHP_SELF']) == "/"))
	{
		require_once("./tehPHP/snowBirdFooter.php");
	}
	else
	{
		require_once("../tehPHP/snowBirdFooter.php");
	}
?>