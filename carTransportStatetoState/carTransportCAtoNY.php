<?php
	$pageTitle = "Snowbird Hauling - Transport Car from California to New York";
	$pageKeywords = "transport a car from california to new york, cost to transport car from california to new york,transport a auto from california to new york, cost to transport auto from california to new york,transport a automobile from california to new york, cost to transport automobile from california to new york,transport a vehicle from california to new york, cost to transport vehicle from california to new york,transport a truck from california to new york, cost to transport truck from california to new york,transporting a car from california to new york, cost to transporting car from california to new york,transporting a auto from california to new york, cost to transporting auto from california to new york,transporting a automobile from california to new york, cost to transporting automobile from california to new york,transporting a vehicle from california to new york, cost to transporting vehicle from california to new york,transporting a truck from california to new york, cost to transporting truck from california to new york,ship a car from california to new york, cost to ship car from california to new york,ship a auto from california to new york, cost to ship auto from california to new york,ship a automobile from california to new york, cost to ship automobile from california to new york,ship a vehicle from california to new york, cost to ship vehicle from california to new york,ship a truck from california to new york, cost to ship truck from california to new york,shipping a car from california to new york, cost to shipping car from california to new york,shipping a auto from california to new york, cost to shipping auto from california to new york,shipping a automobile from california to new york, cost to shipping automobile from california to new york,shipping a vehicle from california to new york, cost to shipping vehicle from california to new york,shipping a truck from california to new york, cost to shipping truck from california to new york,haul a car from california to new york, cost to haul car from california to new york,haul a auto from california to new york, cost to haul auto from california to new york,haul a automobile from california to new york, cost to haul automobile from california to new york,haul a vehicle from california to new york, cost to haul vehicle from california to new york,haul a truck from california to new york, cost to haul truck from california to new york,hauling a car from california to new york, cost to hauling car from california to new york,hauling a auto from california to new york, cost to hauling auto from california to new york,hauling a automobile from california to new york, cost to hauling automobile from california to new york,hauling a vehicle from california to new york, cost to hauling vehicle from california to new york,hauling a truck from california to new york, cost to hauling truck from california to new york,transport a car from ca to ny, cost to transport car from ca to ny,transport a auto from ca to ny, cost to transport auto from ca to ny,transport a automobile from ca to ny, cost to transport automobile from ca to ny,transport a vehicle from ca to ny, cost to transport vehicle from ca to ny,transport a truck from ca to ny, cost to transport truck from ca to ny,transporting a car from ca to ny, cost to transporting car from ca to ny,transporting a auto from ca to ny, cost to transporting auto from ca to ny,transporting a automobile from ca to ny, cost to transporting automobile from ca to ny,transporting a vehicle from ca to ny, cost to transporting vehicle from ca to ny,transporting a truck from ca to ny, cost to transporting truck from ca to ny,ship a car from ca to ny, cost to ship car from ca to ny,ship a auto from ca to ny, cost to ship auto from ca to ny,ship a automobile from ca to ny, cost to ship automobile from ca to ny,ship a vehicle from ca to ny, cost to ship vehicle from ca to ny,ship a truck from ca to ny, cost to ship truck from ca to ny,shipping a car from ca to ny, cost to shipping car from ca to ny,shipping a auto from ca to ny, cost to shipping auto from ca to ny,shipping a automobile from ca to ny, cost to shipping automobile from ca to ny,shipping a vehicle from ca to ny, cost to shipping vehicle from ca to ny,shipping a truck from ca to ny, cost to shipping truck from ca to ny,haul a car from ca to ny, cost to haul car from ca to ny,haul a auto from ca to ny, cost to haul auto from ca to ny,haul a automobile from ca to ny, cost to haul automobile from ca to ny,haul a vehicle from ca to ny, cost to haul vehicle from ca to ny,haul a truck from ca to ny, cost to haul truck from ca to ny,hauling a car from ca to ny, cost to hauling car from ca to ny,hauling a auto from ca to ny, cost to hauling auto from ca to ny,hauling a automobile from ca to ny, cost to hauling automobile from ca to ny,hauling a vehicle from ca to ny, cost to hauling vehicle from ca to ny,hauling a truck from ca to ny, cost to hauling truck from ca to ny";
	$pageDesc = "Snowbord hauling - Specializing in transporting your car from California to New York!";
	require_once("../tehPHP/snowBirdHeader.php");
?>

<div class="snowbirdBG whiteText" style="background: url(<?php echo $tehAbsoluteURL; ?>layout/backgrounds/nyToFloridaCarrier.jpg) no-repeat top center;">
	<div class="centerWrap whiteBG stdBoxShadowOnColorBG contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentFormShell stdBoxShadow">
					<?php
						require_once("../tehPHP/tehRefferalForm.php")
					?>
				</div>
				<div class="contentTitle bold">
					Transport A Car from California to New York
				</div>
				<div class="contentSubTitle grayText">
					From the Golden State to the Empire State
				</div>
				<br />
				<p class="grayText contentParagraph">
					Transporting your vehicle from California to New York is a 2900+ mile adventure. The majority of the trek stretches across I-80 and may take up to 44-50 hours to complete at an average speed of 65mph.  
				</p>
				<p class="grayText contentParagraph">
					The trek is mostly a straight shot across America's southern heartland. And depending on your exact pick-up location and destination may offer you a look at every american climate zone from deserts, to alpine mountains, and forests.
				</p>
				<div class="contentPropaganda snowBirdBlue">
					Transport your car from California to North Carolina! - No Risk
				</div>
				<p class="grayText contentParagraph">
					With just 3 easy steps. You'll be on your way to your destination to meet your vehicle.
				</p>
				<ul class="contentListSteps">
					<li>
						<a class="contentListCircleShell">
							<span>1</span> Complete our Free Quote form
						</a>
					</li>
					<li>
						<a class="contentListCircleShell">
							<span>2</span> We pick up your vehicle.
						</a>
					</li>
					<li>
						<a class="contentListCircleShell">
							<span>3</span> We deliver it to your destination.
						</a>
					</li>
				</ul>
				<br /><br /><br /><br /><br /><br /><br />
				<div class="contentMiniTitle bold">
					Cost to transport from California to New York
				</div>
				<div class="contentMiniTitle bold">
					Cost to ship a car from California to New York
				</div>
				<p class="grayText contentParagraph">
					Pricing  varies according to availability and demand. There are literally thousands of car carriers with vacant spots probably traveling back and forth between California and New York. At Snowbird, we try to contact those drivers and fill up their carriers so that our customers enjoy a reduce cost car transport.
				</p>
				<div class="row snowBirdDisplayTextSize snowBirdBlue">
					<div class="col-lg-6 bold col-md-6">
						Route
					</div>
					<div class="col-lg-2 bold col-md-2">
						Car
					</div>
					<div class="col-lg-2 bold col-md-2">
						SUV
					</div>
					<div class="col-lg-2 bold col-md-2">
						Exotic
					</div>
				</div>
				<div class="row snowBirdDisplayTextSize snowBirdBlue">
					<div class="col-lg-6 col-md-6">
						Los Angeles <i class="fa fa-arrow-right" aria-hidden="true"></i> New York 
					</div>
					<div class="col-lg-2 col-md-2">
						$1139
					</div>
					<div class="col-lg-2 col-md-2">
						$1299
					</div>
					<div class="col-lg-2 col-md-2">
						$1529
					</div>
					<div class="col-lg-6 col-md-6">
						San Francisco <i class="fa fa-arrow-right" aria-hidden="true"></i> Buffalo
					</div>
					<div class="col-lg-2 col-md-2">
						$1269
					</div>
					<div class="col-lg-2 col-md-2">
						$1389
					</div>
					<div class="col-lg-2 col-md-2">
						$1609
					</div>
				</div>
				<br /><br />
			</div>
		</div>
	</div>
</div>



<?php
	//if (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1")
	if( (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1") || (dirname($_SERVER['PHP_SELF']) == "/"))
	{
		require_once("./tehPHP/snowBirdFooter.php");
	}
	else
	{
		require_once("../tehPHP/snowBirdFooter.php");
	}
?>