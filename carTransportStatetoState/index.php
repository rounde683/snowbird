<?php
	$pageTitle = "Snowbird Hauling - State to State Auto Transport";
	$pageKeywords = "state to state car movers,state to state car transport,transport a car from one state to another,transport my car from one state to another,transport my car to another state,transport car to another state,transport car state to state,state to state auto transport,transport a auto from one state to another,transport my auto from one state to another,transport my auto to another state,transport auto to another state,transport auto state to state,state to state automobile transport,transport a automobile from one state to another,transport my automobile from one state to another,transport my automobile to another state,transport automobile to another state,transport automobile state to state,state to state vehicle transport,transport a vehicle from one state to another,transport my vehicle from one state to another,transport my vehicle to another state,transport vehicle to another state,transport vehicle state to state,state to state truck transport,transport a truck from one state to another,transport my truck from one state to another,transport my truck to another state,transport truck to another state,transport truck state to state,state to state car transporting,transporting a car from one state to another,transporting my car from one state to another,transporting my car to another state,transporting car to another state,transporting car state to state,state to state auto transporting,transporting a auto from one state to another,transporting my auto from one state to another,transporting my auto to another state,transporting auto to another state,transporting auto state to state,state to state automobile transporting,transporting a automobile from one state to another,transporting my automobile from one state to another,transporting my automobile to another state,transporting automobile to another state,transporting automobile state to state,state to state vehicle transporting,transporting a vehicle from one state to another,transporting my vehicle from one state to another,transporting my vehicle to another state,transporting vehicle to another state,transporting vehicle state to state,state to state truck transporting,transporting a truck from one state to another,transporting my truck from one state to another,transporting my truck to another state,transporting truck to another state,transporting truck state to state,state to state car haul,haul a car from one state to another,haul my car from one state to another,haul my car to another state,haul car to another state,haul car state to state,state to state auto haul,haul a auto from one state to another,haul my auto from one state to another,haul my auto to another state,haul auto to another state,haul auto state to state,state to state automobile haul,haul a automobile from one state to another,haul my automobile from one state to another,haul my automobile to another state,haul automobile to another state,haul automobile state to state,state to state vehicle haul,haul a vehicle from one state to another,haul my vehicle from one state to another,haul my vehicle to another state,haul vehicle to another state,haul vehicle state to state,state to state truck haul,haul a truck from one state to another,haul my truck from one state to another,haul my truck to another state,haul truck to another state,haul truck state to state,state to state car hauling,hauling a car from one state to another,hauling my car from one state to another,hauling my car to another state,hauling car to another state,hauling car state to state,state to state auto hauling,hauling a auto from one state to another,hauling my auto from one state to another,hauling my auto to another state,hauling auto to another state,hauling auto state to state,state to state automobile hauling,hauling a automobile from one state to another,hauling my automobile from one state to another,hauling my automobile to another state,hauling automobile to another state,hauling automobile state to state,state to state vehicle hauling,hauling a vehicle from one state to another,hauling my vehicle from one state to another,hauling my vehicle to another state,hauling vehicle to another state,hauling vehicle state to state,state to state truck hauling,hauling a truck from one state to another,hauling my truck from one state to another,hauling my truck to another state,hauling truck to another state,hauling truck state to state";
	$pageDesc = "Snowbord hauling - Specializing in State to State Auto Transporting. We take away the hassle from transporting your car state to state.";
	require_once("../tehPHP/snowBirdHeader.php");
?>

<div class="snowbirdBG whiteText" style="background: url(<?php echo $tehAbsoluteURL; ?>layout/backgrounds/nyToFloridaCarrier.jpg) no-repeat top center;">
	<div class="centerWrap whiteBG stdBoxShadowOnColorBG contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentFormShell stdBoxShadow">
					<?php
						require_once("../tehPHP/tehRefferalForm.php")
					?>
				</div>
				<div class="contentTitle bold">
					State to State Car Transport
				</div>
				<div class="contentSubTitle grayText">
					Let Snowbird do the heavy lifting.
				</div>
				<br />
				<p class="grayText contentParagraph">
					You have two choices when moving a car or vehicle from state to state. Either drive it your self, or hire a state to state car transportation service. Hiring a car transportation service with state to state services is now a hassle free process thanks to the internet.
				</p>
				<p class="grayText contentParagraph">
					The process for hiring a car shipping service is simple. Just fill out our no obligation quote form and we'll get pricing quotes from thousands of car hauling service providers in your area. Once you've selected a price and company, we'll arrange the whole thing including pick up and delivery directions.
				</p>
				<p class="grayText contentParagraph">
					At Snowbird, it's our job to match customers like you with independent and affiliated car haulers capable of delivering your precious vehicle safely from state to state.
				</p>
				<div class="contentMiniTitle bold">
					Benefits of Hiring State to State Transportation
				</div>
				<br />
				<ul class="contentList">
					<li>
						No-Risk, Licensed and Insured
					</li>
					<li>
						Avoid the wear and tear on your vehicle
					</li>
					<li>
						Time is money, Save Money
					</li>
				</ul>
				<!--
				<img class="contentIMG" width="300" title="Car Transport NY to FL" alt="NY, FL Sign" src="<?php echo $tehAbsoluteURL; ?>layout/images/NYtoFL.jpg" />
				-->
			</div>
		</div>
	</div>
</div>



<?php
	//if (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1")
	if( (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1") || (dirname($_SERVER['PHP_SELF']) == "/"))
	{
		require_once("./tehPHP/snowBirdFooter.php");
	}
	else
	{
		require_once("../tehPHP/snowBirdFooter.php");
	}
?>