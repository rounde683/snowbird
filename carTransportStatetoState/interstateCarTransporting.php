<?php
	$pageTitle = "Snowbird Hauling - Interstate Car Transporting";
	$pageKeywords = "interstate car transport,interstate cars transport,interstate auto transport,interstate autos transport,interstate automobile transport,interstate automobiles transport,interstate vehicle transport,interstate vehicles transport,interstate truck transport,interstate trucks transport,interstate car transporting,interstate cars transporting,interstate auto transporting,interstate autos transporting,interstate automobile transporting,interstate automobiles transporting,interstate vehicle transporting,interstate vehicles transporting,interstate truck transporting,interstate trucks transporting,interstate car haul,interstate cars haul,interstate auto haul,interstate autos haul,interstate automobile haul,interstate automobiles haul,interstate vehicle haul,interstate vehicles haul,interstate truck haul,interstate trucks haul,interstate car hauling,interstate cars hauling,interstate auto hauling,interstate autos hauling,interstate automobile hauling,interstate automobiles hauling,interstate vehicle hauling,interstate vehicles hauling,interstate truck hauling,interstate trucks hauling,interstate car ship,interstate cars ship,interstate auto ship,interstate autos ship,interstate automobile ship,interstate automobiles ship,interstate vehicle ship,interstate vehicles ship,interstate truck ship,interstate trucks ship,interstate car shipping,interstate cars shipping,interstate auto shipping,interstate autos shipping,interstate automobile shipping,interstate automobiles shipping,interstate vehicle shipping,interstate vehicles shipping,interstate truck shipping,interstate trucks shipping";
	$pageDesc = "Interstate car transporting services provided by Snowbird Hauling. We take away the hassle from transporting your car across the states.";
	require_once("../tehPHP/snowBirdHeader.php");
?>

<div class="snowbirdBG whiteText" style="background: url(<?php echo $tehAbsoluteURL; ?>layout/backgrounds/nyToFloridaCarrier.jpg) no-repeat top center;">
	<div class="centerWrap whiteBG stdBoxShadowOnColorBG contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentFormShell stdBoxShadow">
					<?php
						require_once("../tehPHP/tehRefferalForm.php")
					?>
				</div>
				<div class="contentTitle bold">
					Interstate Car Transporting
				</div>
				<div class="contentSubTitle grayText">
					Let Snowbird transport your vehicle between states
				</div>
				<br />
				<p class="grayText contentParagraph">
					With the rising price of gas, interstate car transporting is a growing industry. It's a low-cost, safe alternative to driving yourself across statelines. A professional driver will pick up your vehicle and then deliver it safely to your interstate destination granting you more free time to make other travel arrangements or jump on a plane then sit back and relax.
				</p>
				<p class="grayText contentParagraph">
					
				</p>
				<div class="contentMiniTitle bold">
					Preparing your car for Interstate Transportation
				</div>
				<p class="grayText contentParagraph">
					Some preparation is required before sending your beloved vehicle on it's merry journey. For starters, have a thorough examination done of the vehicle. Take photos so that any new damage can be clearly documented and sent to the hauling company's insurance.
				<p class="grayText contentParagraph">
					Ideally, a vehicle should only be carrying a quarter tank of gas to limit weight and limit the amount of hazardous material (fuel) being transported.
				</p>
				<p class="grayText contentParagraph">
					The USDOT (Department of Transportation) requires that all keys be turned over to he hauling company while the vehicle is transported. Some companies may require the car to be emptied of all personal contents, others may allow you to fill the vehicle with some personal belongings. These questions should be asked prior to signing a contract.
				</p>
				<div class="contentMiniTitle bold">
					Car Preparation Quick List:
				</div>
				<br />
				<ul class="contentList">
					<li>
						Inspect the vehicle, take photos if necessary.
					</li>
					<li>
						Run the gas tank down to a quarter tank of fuel.
					</li>
					<li>
						Have all keys ready to turn over.
					</li>
					<li>
						Ask questions about the car contents.
					</li>
					<li>
						Check delivery times, and exchange contact information with the driver or representative.
					</li>
				</ul>
				<!--
				<img class="contentIMG" width="300" title="Car Transport NY to FL" alt="NY, FL Sign" src="<?php echo $tehAbsoluteURL; ?>layout/images/NYtoFL.jpg" />
				-->
			</div>
		</div>
	</div>
</div>



<?php
	//if (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1")
	if( (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1") || (dirname($_SERVER['PHP_SELF']) == "/"))
	{
		require_once("./tehPHP/snowBirdFooter.php");
	}
	else
	{
		require_once("../tehPHP/snowBirdFooter.php");
	}
?>