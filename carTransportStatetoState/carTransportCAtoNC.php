<?php
	$pageTitle = "Snowbird Hauling - Transport Car from California to North Carolina";
	$pageKeywords = "car transport california to north carolina,auto transport california to north carolina,automobile transport california to north carolina,vehicle transport california to north carolina,truck transport california to north carolina,car trainsporting california to north carolina,auto trainsporting california to north carolina,automobile trainsporting california to north carolina,vehicle trainsporting california to north carolina,truck trainsporting california to north carolina,car transportation california to north carolina,auto transportation california to north carolina,automobile transportation california to north carolina,vehicle transportation california to north carolina,truck transportation california to north carolina,car ship california to north carolina,auto ship california to north carolina,automobile ship california to north carolina,vehicle ship california to north carolina,truck ship california to north carolina,car shipping california to north carolina,auto shipping california to north carolina,automobile shipping california to north carolina,vehicle shipping california to north carolina,truck shipping california to north carolina,car haul california to north carolina,auto haul california to north carolina,automobile haul california to north carolina,vehicle haul california to north carolina,truck haul california to north carolina,car hauling california to north carolina,auto hauling california to north carolina,automobile hauling california to north carolina,vehicle hauling california to north carolina,truck hauling california to north carolina,car transport CA to NC,auto transport CA to NC,automobile transport CA to NC,vehicle transport CA to NC,truck transport CA to NC,car trainsporting CA to NC,auto trainsporting CA to NC,automobile trainsporting CA to NC,vehicle trainsporting CA to NC,truck trainsporting CA to NC,car transportation CA to NC,auto transportation CA to NC,automobile transportation CA to NC,vehicle transportation CA to NC,truck transportation CA to NC,car ship CA to NC,auto ship CA to NC,automobile ship CA to NC,vehicle ship CA to NC,truck ship CA to NC,car shipping CA to NC,auto shipping CA to NC,automobile shipping CA to NC,vehicle shipping CA to NC,truck shipping CA to NC,car haul CA to NC,auto haul CA to NC,automobile haul CA to NC,vehicle haul CA to NC,truck haul CA to NC,car hauling CA to NC,auto hauling CA to NC,automobile hauling CA to NC,vehicle hauling CA to NC,truck hauling CA to NC";
	$pageDesc = "Snowbord hauling - If you need to transport your car from California to North Carolina, look no further than Snowbird Hauling. We've got pricing information and everything you need to know.";
	require_once("../tehPHP/snowBirdHeader.php");
?>

<div class="snowbirdBG whiteText" style="background: url(<?php echo $tehAbsoluteURL; ?>layout/backgrounds/nyToFloridaCarrier.jpg) no-repeat top center;">
	<div class="centerWrap whiteBG stdBoxShadowOnColorBG contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentFormShell stdBoxShadow">
					<?php
						require_once("../tehPHP/tehRefferalForm.php")
					?>
				</div>
				<div class="contentTitle bold">
					Transport Car from California to Florida
				</div>
				<div class="contentSubTitle grayText">
					From the Golden State to the Tar Heel State
				</div>
				<br />
				<p class="grayText contentParagraph">
					Transporting your vehicle from California to North Carolina means you've got a 2,700 mile drive across the country in store for you. This could mean a 40+ hour endeavor if you plan on making the drive yourself. Or. You could hire a car transportation company and they will pick up your car and deliver it from California to North Carolina.
				</p>
				<p class="grayText contentParagraph">
					The trek is mostly a straight shot across America's southern heartland. And depending on your exact pick-up location and destination may offer you a look at every american climate zone from deserts, to alpine mountains, and forests.
				</p>
				<div class="contentPropaganda snowBirdBlue">
					Transport your car from California to North Carolina! - No Risk
				</div>
				<p class="grayText contentParagraph">
					With just 3 easy steps. You'll be on your way to your destination to meet your vehicle.
				</p>
				<ul class="contentListSteps">
					<li>
						<a class="contentListCircleShell">
							<span>1</span> Complete our Free Quote form
						</a>
					</li>
					<li>
						<a class="contentListCircleShell">
							<span>2</span> We pick up your vehicle.
						</a>
					</li>
					<li>
						<a class="contentListCircleShell">
							<span>3</span> We deliver it to your destination.
						</a>
					</li>
				</ul>
				<br /><br /><br /><br /><br /><br /><br />
				<div class="contentMiniTitle bold">
					Cost to transport from Florida to California
				</div>
				<div class="contentSubTitle grayText">
					Prices vary according to season, demand, and availability.
				</div>
				<p class="grayText contentParagraph">
					Pricing  varies according to availability and demand. There are literally thousands of car carriers with vacant spots probably traveling back and forth between Florida and California. At Snowbird, we try to contact those drivers and fill up their carriers so that our customers enjoy a reduce cost car transport.
				</p>
				<div class="contentMiniTitle bold">
					Cost to ship a car from California to North Carolina
				</div>
				<p class="grayText contentParagraph">
					THe prices below are offered as estimates only. Pricing may vary according to availability, demand, season, and vehicle type.
				</p>
				<div class="row snowBirdDisplayTextSize snowBirdBlue">
					<div class="col-lg-6 bold col-md-6">
						Route
					</div>
					<div class="col-lg-2 bold col-md-2">
						Car
					</div>
					<div class="col-lg-2 bold col-md-2">
						SUV
					</div>
					<div class="col-lg-2 bold col-md-2">
						Exotic
					</div>
				</div>
				<div class="row snowBirdDisplayTextSize snowBirdBlue">
					<div class="col-lg-6 col-md-6">
						Los Angeles <i class="fa fa-arrow-right" aria-hidden="true"></i> Raleigh 
					</div>
					<div class="col-lg-2 col-md-2">
						$1039
					</div>
					<div class="col-lg-2 col-md-2">
						$1189
					</div>
					<div class="col-lg-2 col-md-2">
						$1399
					</div>
					<div class="col-lg-6 col-md-6">
						San Francisco <i class="fa fa-arrow-right" aria-hidden="true"></i> Raleigh
					</div>
					<div class="col-lg-2 col-md-2">
						$1269
					</div>
					<div class="col-lg-2 col-md-2">
						$1349
					</div>
					<div class="col-lg-2 col-md-2">
						$1619
					</div>
				</div>
				<br /><br />
			</div>
		</div>
	</div>
</div>



<?php
	//if (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1")
	if( (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1") || (dirname($_SERVER['PHP_SELF']) == "/"))
	{
		require_once("./tehPHP/snowBirdFooter.php");
	}
	else
	{
		require_once("../tehPHP/snowBirdFooter.php");
	}
?>