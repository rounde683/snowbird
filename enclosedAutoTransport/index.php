<?php
	$pageTitle = "Snowbird Hauling - Enclosed Auto Transport";
	$pageKeywords = "enclosed carrier car transport,enclosed car transport,enclosed carrier auto transport,enclosed auto transport,enclosed carrier automobile transport,enclosed automobile transport,enclosed carrier vehicle transport,enclosed vehicle transport,enclosed carrier truck transport,enclosed truck transport,enclosed carrier car transporting,enclosed car transporting,enclosed carrier auto transporting,enclosed auto transporting,enclosed carrier automobile transporting,enclosed automobile transporting,enclosed carrier vehicle transporting,enclosed vehicle transporting,enclosed carrier truck transporting,enclosed truck transporting,enclosed carrier car haul,enclosed car haul,enclosed carrier auto haul,enclosed auto haul,enclosed carrier automobile haul,enclosed automobile haul,enclosed carrier vehicle haul,enclosed vehicle haul,enclosed carrier truck haul,enclosed truck haul,enclosed carrier car hauling,enclosed car hauling,enclosed carrier auto hauling,enclosed auto hauling,enclosed carrier automobile hauling,enclosed automobile hauling,enclosed carrier vehicle hauling,enclosed vehicle hauling,enclosed carrier truck hauling,enclosed truck hauling,enclosed carrier car ship,enclosed car ship,enclosed carrier auto ship,enclosed auto ship,enclosed carrier automobile ship,enclosed automobile ship,enclosed carrier vehicle ship,enclosed vehicle ship,enclosed carrier truck ship,enclosed truck ship,enclosed carrier car shipping,enclosed car shipping,enclosed carrier auto shipping,enclosed auto shipping,enclosed carrier automobile shipping,enclosed automobile shipping,enclosed carrier vehicle shipping,enclosed vehicle shipping,enclosed carrier truck shipping,enclosed truck shipping";
	$pageDesc = "Snowbord hauling - Offering enclosed auto transport services throughout the US.";
	require_once("../tehPHP/snowBirdHeader.php");
?>

<div class="snowbirdBG whiteText" style="background: url(<?php echo $tehAbsoluteURL; ?>layout/backgrounds/nyToFloridaCarrier.jpg) no-repeat top center;">
	<div class="centerWrap whiteBG stdBoxShadowOnColorBG contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentFormShell stdBoxShadow">
					<?php
						require_once("../tehPHP/tehRefferalForm.php")
					?>
				</div>
				<div class="contentTitle bold">
					Snowbird Enclosed Auto Transport
				</div>
				<div class="contentSubTitle grayText">
					Enclosed Auto Transport Services offering complete auto protection while in transit.
				</div>
				<br />
				<p class="grayText contentParagraph">
					Enclsosed auto transportation is available across the country offering complete auto protection while your car is shipped from state to state or across the entire country. Enclosure offers complete protection from rain, snow, and ice. 
				<p class="grayText contentParagraph">
					Snowbird Hauling offers enclosed car transportation services. We'll pick up your car at your door and deliver it to where ever is required with no-risk, no worry, and no hassle. 
				</p>
				<div class="contentPropaganda snowBirdBlue">
					No Hassle - Enclosed Auto Transportation Services
				</div>
				<div class="contentMiniTitle bold">
					The Cost of Enclosed Auto Transportation
				</div>
				<p class="grayText contentParagraph">
					Only about 2-5% of haulers have enclosures. This tends to place them in high demand raising the cost to transport by about 35% - 50%.
				</p>
				<div class="row snowBirdDisplayTextSize snowBirdBlue">
					<div class="col-lg-8 bold">
						Route
					</div>
					<div class="col-lg-2 bold">
						Open
					</div>
					<div class="col-lg-2 bold">
						Enclosed
					</div>
				</div>
				<div class="row snowBirdDisplayTextSize snowBirdBlue">
					<div class="col-lg-8">
						New York <i class="fa fa-arrow-right" aria-hidden="true"></i> Boca Raton 
					</div>
					<div class="col-lg-2">
						$749
					</div>
					<div class="col-lg-2">
						$1011
					</div>
				</div>
				<div class="row snowBirdDisplayTextSize snowBirdBlue">
					<div class="col-lg-8">
						LA <i class="fa fa-arrow-right" aria-hidden="true"></i> New York
					</div>
					<div class="col-lg-2">
						$1349
					</div>
					<div class="col-lg-2">
						$1821
					</div>
				</div>
				<div class="row snowBirdDisplayTextSize snowBirdBlue">
					<div class="col-lg-8">
						Boston <i class="fa fa-arrow-right" aria-hidden="true"></i> Charleston
					</div>
					<div class="col-lg-2">
						$689
					</div>
					<div class="col-lg-2">
						$930
					</div>
				</div>
				<div class="row snowBirdDisplayTextSize snowBirdBlue">
					<div class="col-lg-8">
						Detroit <i class="fa fa-arrow-right" aria-hidden="true"></i> San Diego
					</div>
					<div class="col-lg-2">
						$989
					</div>
					<div class="col-lg-2">
						$1335
					</div>
				</div>
				<div class="row snowBirdDisplayTextSize snowBirdBlue">
					<div class="col-lg-8">
						New York <i class="fa fa-arrow-right" aria-hidden="true"></i> Seattle
					</div>
					<div class="col-lg-2">
						$1129
					</div>
					<div class="col-lg-2">
						$1524
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<?php
	//if (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1")
	if( (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1") || (dirname($_SERVER['PHP_SELF']) == "/"))
	{
		require_once("./tehPHP/snowBirdFooter.php");
	}
	else
	{
		require_once("../tehPHP/snowBirdFooter.php");
	}
?>