<?php
	$pageTitle = "Snowbird Hauling - Miami Car Transport";
	$pageKeywords = "auto transport miami,auto transport miami fl,miami auto transport,auto transport miami florida,automobile transport miami,automobile transport miami fl,miami automobile transport,automobile transport miami florida,car transport miami,car transport miami fl,miami car transport,car transport miami florida,vehicle transport miami,vehicle transport miami fl,miami vehicle transport,vehicle transport miami florida,truck transport miami,truck transport miami fl,miami truck transport,truck transport miami florida,auto transporting miami,auto transporting miami fl,miami auto transporting,auto transporting miami florida,automobile transporting miami,automobile transporting miami fl,miami automobile transporting,automobile transporting miami florida,car transporting miami,car transporting miami fl,miami car transporting,car transporting miami florida,vehicle transporting miami,vehicle transporting miami fl,miami vehicle transporting,vehicle transporting miami florida,truck transporting miami,truck transporting miami fl,miami truck transporting,truck transporting miami florida,auto haul miami,auto haul miami fl,miami auto haul,auto haul miami florida,automobile haul miami,automobile haul miami fl,miami automobile haul,automobile haul miami florida,car haul miami,car haul miami fl,miami car haul,car haul miami florida,vehicle haul miami,vehicle haul miami fl,miami vehicle haul,vehicle haul miami florida,truck haul miami,truck haul miami fl,miami truck haul,truck haul miami florida,auto hauling miami,auto hauling miami fl,miami auto hauling,auto hauling miami florida,automobile hauling miami,automobile hauling miami fl,miami automobile hauling,automobile hauling miami florida,car hauling miami,car hauling miami fl,miami car hauling,car hauling miami florida,vehicle hauling miami,vehicle hauling miami fl,miami vehicle hauling,vehicle hauling miami florida,truck hauling miami,truck hauling miami fl,miami truck hauling,truck hauling miami florida,auto ship miami,auto ship miami fl,miami auto ship,auto ship miami florida,automobile ship miami,automobile ship miami fl,miami automobile ship,automobile ship miami florida,car ship miami,car ship miami fl,miami car ship,car ship miami florida,vehicle ship miami,vehicle ship miami fl,miami vehicle ship,vehicle ship miami florida,truck ship miami,truck ship miami fl,miami truck ship,truck ship miami florida,auto shipping miami,auto shipping miami fl,miami auto shipping,auto shipping miami florida,automobile shipping miami,automobile shipping miami fl,miami automobile shipping,automobile shipping miami florida,car shipping miami,car shipping miami fl,miami car shipping,car shipping miami florida,vehicle shipping miami,vehicle shipping miami fl,miami vehicle shipping,vehicle shipping miami florida,truck shipping miami,truck shipping miami fl,miami truck shipping,truck shipping miami florida";
	$pageDesc = "Snowbord hauling - If you need to transport your car from or to Miami, look no further than Snowbird Hauling!";
	require_once("../tehPHP/snowBirdHeader.php");
?>

<div class="snowbirdBG whiteText" style="background: url(<?php echo $tehAbsoluteURL; ?>layout/backgrounds/nyToFloridaCarrier.jpg) no-repeat top center;">
	<div class="centerWrap whiteBG stdBoxShadowOnColorBG contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentFormShell stdBoxShadow">
					<?php
						require_once("../tehPHP/tehRefferalForm.php")
					?>
				</div>
				<div class="contentTitle bold">
					Car Transport Miami
				</div>
				<div class="contentSubTitle grayText">
					Miami &amp; Miami Beach Car Transport
				</div>
				<br />
				<p class="grayText contentParagraph">
					Miami is a city like no other. Snowbirds have been flocking to the city druing the winter months for nearly a century. Most of whom will pay to transport their vehicles down for their longterm stays home away from homes. It's estiated that nearly 900,000 tourists flock to south Florida during those northern wintery months.
				</p>
				<div class="contentPropaganda snowBirdBlue">
					Free, No Obligation quote for Miami Auto Transporting Services
				</div>
				<div class="contentMiniTitle bold">
					Miami Auto Transportation Estimates
				</div>
				<p class="grayText contentParagraph">
					As you may expect, pricing varies upon availability, season, and the vehicle.
				</p>
				<div class="row snowBirdDisplayTextSize snowBirdBlue">
					<div class="col-lg-8 bold">
						Route
					</div>
					<div class="col-lg-2 bold">
						Open
					</div>
					<div class="col-lg-2 bold">
						Enclosed
					</div>
				</div>
				<div class="row snowBirdDisplayTextSize snowBirdBlue">
					<div class="col-lg-8">
						New York <i class="fa fa-arrow-right" aria-hidden="true"></i> Miami
					</div>
					<div class="col-lg-2">
						$999
					</div>
					<div class="col-lg-2">
						$1299
					</div>
				</div>
				<div class="row snowBirdDisplayTextSize snowBirdBlue">
					<div class="col-lg-8">
						DC <i class="fa fa-arrow-right" aria-hidden="true"></i> Miami
					</div>
					<div class="col-lg-2">
						$749
					</div>
					<div class="col-lg-2">
						$1021
					</div>
				</div>
				<div class="row snowBirdDisplayTextSize snowBirdBlue">
					<div class="col-lg-8">
						Boston <i class="fa fa-arrow-right" aria-hidden="true"></i> Miami
					</div>
					<div class="col-lg-2">
						$869
					</div>
					<div class="col-lg-2">
						$1189
					</div>
				</div>
				<div class="row snowBirdDisplayTextSize snowBirdBlue">
					<div class="col-lg-8">
						Charleston <i class="fa fa-arrow-right" aria-hidden="true"></i> Miami
					</div>
					<div class="col-lg-2">
						$549
					</div>
					<div class="col-lg-2">
						$709
					</div>
				</div>
				<div class="row snowBirdDisplayTextSize snowBirdBlue">
					<div class="col-lg-8">
						Atlanta <i class="fa fa-arrow-right" aria-hidden="true"></i> Miami
					</div>
					<div class="col-lg-2">
						$429
					</div>
					<div class="col-lg-2">
						$589
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<?php
	//if (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1")
	if( (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1") || (dirname($_SERVER['PHP_SELF']) == "/"))
	{
		require_once("./tehPHP/snowBirdFooter.php");
	}
	else
	{
		require_once("../tehPHP/snowBirdFooter.php");
	}
?>