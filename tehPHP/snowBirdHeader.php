<?php
	$tehAbsoluteURL = "http://snowbirdhauling.com/";
	$tehAbsoluteURL = "http://216.172.105.50/~snowbird/";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"	xml:lang="en">
	<head>
		<script src="<?php echo $tehAbsoluteURL; ?>js/jquery-3.2.1.min.js" type="text/javascript" charset="utf-8"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo $tehAbsoluteURL; ?>css/default.css" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="<?php echo $tehAbsoluteURL; ?>css/font-awesome_min.css" />

		<link rel="SHORTCUT ICON" href="<?php echo $tehAbsoluteURL; ?>favicon.ico" />

		<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Shadows+Into+Light" rel="stylesheet">

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">

		<title><?php echo $pageTitle; ?></title>
		<meta http-equiv="Content-Type"	content="text/html; charset=iso-8859-1" />
		<meta name='keywords' content="<?php echo $pageKeywords; ?>" />
		<meta name='description' content="<?php echo $pageDesc; ?>" />
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106912309-2"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-106912309-2');
		</script>

	</head>

	<body>
		<div class="snowBirdStickyHeader" id="snowBirdStickyHeaderID">
			<span class="mainMenuLogoShell">
            	<a class="snowBirdBlueText" href="<?php echo $tehAbsoluteURL; ?>">
	                <img style="float: left;" height="40" alt="Snowbird Hauling Logo" title="Snowbird Hauling: Hauling Cars is what we do."  src="<?php echo $tehAbsoluteURL; ?>layout/logos/snowBirdLogo.png" />
	                <span class="mainMenuLogoText bold" style="font-size: 1.4em;">
	                    Snowbird Hauling
	                </span>
	            </a>
	        </span>
		</div>
			<?php
				if( (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1") || (dirname($_SERVER['PHP_SELF']) == "/"))
				//if(dirname($_SERVER['PHP_SELF']) == "/")
				{
					require_once("./tehPHP/mainMenu.php");
				}
				else
				{
					require_once("../tehPHP/mainMenu.php");
				}
			?>