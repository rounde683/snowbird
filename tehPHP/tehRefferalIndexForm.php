
<script src="<?php echo $tehAbsoluteURL; ?>js/parsley.min.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo $tehAbsoluteURL; ?>js/jquery.easy-autocomplete.js" type="text/javascript" charset="utf-8"></script>

<link rel="stylesheet" type="text/css" href="<?php echo $tehAbsoluteURL; ?>css/parsley.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $tehAbsoluteURL; ?>css/easy-autocomplete.css" />

<div class="snowBirdSuccessFormMessageShell parsley-success hidden">
	<div class="bold">
		Thank you!
	</div>
	<br />
	<p>
		Your quote has been submitted <b>successfully</b>! Now we'll put one of our specialists to <b>work for you</b> and get back to you within <b>24 hours</b>.
	</p>
	<p>
		Sit back and enjoy!
	</p>
</div>

<form id="snowBirdQuoteForm" method="POST" data-parsley-validate="">
	<fieldset>
		<div class="contactUsFormTitleBlack" style="text-align: left !important;">Route</div>
		<div class="gridShellThirtyThree" style="grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));">
			<div class="gridColumnShell">
				<div class="form-group">
					<input type="text" id="originatingCityZip" class="form-control snowBirdInput" name="originatingCityZip" required="" placeholder="Zip (Originating)" minlength="5" maxlength="44"  data-parsley-trigger="keyup">
				</div>
			</div>
			<div class="gridColumnShell">
				<div class="form-group">
					<input type="text" id="destinationCityZip" class="form-control snowBirdInput" name="destinationCityZip" required="" placeholder="Zip (Destination)" minlength="5" maxlength="44"  data-parsley-trigger="keyup">
				</div>
			</div>
		</div>
		<div class="contactUsFormTitleBlack" style="text-align: left !important;">Contact Info</div>
		<div class="form-group">
			<input type="text" class="form-control snowBirdInput" name="contactName" required="" placeholder="Name" minlength="2" size="32" data-parsley-trigger="keyup">
			<input type="text" data-parsley-type="email" class="form-control snowBirdInput" name="contactEmail" required="" placeholder="youremail@email.com" size="32" data-parsley-trigger="keyup">
			<input type="text" data-parsley-type="digits" class="form-control snowBirdInput" name="contactPhone" required="" placeholder="xxxxxxxxxx" minlength="10" maxlength="10" size="10" data-parsley-trigger="keyup">
		</div>
		<center>
			<button type="submit" onfocus="blur()" class="snowBirdButtonShell" style="background: none; border: none;">
				<a class="snowBirdButton">
					Request Quote
				</a>
			</button>
		</center>
	</fieldset>
</form>



<script type="text/javascript">
	$(function () {
		$('#snowBirdQuoteForm').parsley().on('field:validated', function() {
		var ok = $('.parsley-error').length === 0;
	})
	.on('form:submit', function() {
  		event.preventDefault();
  		var formData = {
            'originatingCityZip'	: $('input[name=originatingCityZip]').val(),
            'destinationCityZip'	: $('input[name=destinationCityZip]').val(),
            'contactName'			: $('input[name=contactName]').val(),
            'contactEmail'			: $('input[name=contactEmail]').val(),
            'contactPhone'			: $('input[name=contactPhone]').val()
        };
  	$.ajax({
	    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
	    url         : '<?php echo $tehAbsoluteURL; ?>tehPHP/formValidations/snowbirdFreeQuoteValidation.php', // the url where we want to POST
	    data        : formData, // our data object
	    dataType    : 'json' // what type of data do we expect back from the server
	})
    // using the done promise callback
    .done(function(data) {
    	$('#snowBirdQuoteForm').toggleClass('hidden');
    	$('.snowBirdFormLogoShell').toggleClass('hidden');
    	$('.snowBirdSuccessFormMessageShell').toggleClass('hidden');
    })

    // using the fail promise callback
    .fail(function(data) {

        // show any errors
        // best to remove for production
        console.log("fail");
        //console.log(data);
        //create generic error message
    });
    return false; // Don't submit form for this demo
  });
});
</script>
<script>
	var optionsOrig = {

	  url: function(phrase) {
	    return "<?php echo $tehAbsoluteURL; ?>tehPHP/getZipCityState.php";
	  },

	  getValue: function(element) {
	    return element.ZIP_CODE +", " + element.CITY + " " + element.STATE_PREFIX;
	  },

	  ajaxSettings: {
	    dataType: "json",
	    method: "POST",
	    data: {
	      dataType: "json"
	    }
	  },

	  preparePostData: function(data) {
	    data.phrase = $("#originatingCityZip").val();
	    return data;
	  },

	  requestDelay: 400
	};
	var optionsDest = {

	  url: function(phrase) {
	    return "<?php echo $tehAbsoluteURL; ?>tehPHP/getZipCityState.php";
	  },

	  getValue: function(element) {
	    return element.ZIP_CODE +", " + element.CITY + " " + element.STATE_PREFIX;
	  },

	  ajaxSettings: {
	    dataType: "json",
	    method: "POST",
	    data: {
	      dataType: "json"
	    }
	  },

	  preparePostData: function(data) {
	    data.phrase = $("#destinationCityZip").val();
	    return data;
	  },

	  requestDelay: 400
	};

	//$("#example-ajax-post").easyAutocomplete(options);
	$("#originatingCityZip").easyAutocomplete(optionsOrig);
	$("#destinationCityZip").easyAutocomplete(optionsDest);
</script>