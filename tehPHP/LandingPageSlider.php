<link href="http://fonts.googleapis.com/css?family=Raleway%3A800%2C500%2C400" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo $tehAbsoluteURL; ?>revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
<link rel="stylesheet" type="text/css" href="<?php echo $tehAbsoluteURL; ?>revolution/css/settings.css">
<link rel="stylesheet" type="text/css" href="<?php echo $tehAbsoluteURL; ?>revolution/css/layers.css">
<link rel="stylesheet" type="text/css" href="<?php echo $tehAbsoluteURL; ?>revolution/css/navigation.css">
<link href="http://fonts.googleapis.com/css?family=Roboto%3A700%2C300" rel="stylesheet" property="stylesheet" type="text/css" media="all" />

<script type="text/javascript" src="<?php echo $tehAbsoluteURL; ?>revolution/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?php echo $tehAbsoluteURL; ?>revolution/js/jquery.themepunch.revolution.min.js"></script>

<!-- SLIDER EXAMPLE -->
<section class="example" style="margin-top: -220px; height: 780px; background: url(<?php echo $tehAbsoluteURL; ?>layout/revTechSlider/mountainRoadCarrier.jpg);">
	<article class="content">
		<div id="rev_slider_1078_2_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="classic4export" data-source="gallery" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
		<!-- START REVOLUTION SLIDER 5.4.1 fullwidth mode -->
		<div id="rev_slider_1078_2" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.1">
			<div class="sliderHeaderShell">
				<div class="centerText">
					<img height="75" alt="Snowbird Hauling Logo" title="Snowbird Hauling: Hauling Cars is what we do."  src="<?php echo $tehAbsoluteURL; ?>layout/logos/snowBirdLogo.png" />
				</div>
				<div class="snowBirdBlue oneHalfText textShadowLight centerText">
					Snowbird Hauling
				</div>
				<div class="whiteText textShadow bold centerText tripleText">
					Hauling Cars is What We Do.
				</div>
			</div>
	<div class="sliderFormShell">
		<?php
			require_once("./tehPHP/tehRefferalIndexForm.php")
		?>
	</div>
<ul>	<!-- SLIDE  -->
	<li data-index="rs-3045" data-transition="no-transition" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="<?php echo $tehAbsoluteURL; ?>layout/revTechSlider/thumbs/CustomAutoShopThumb.jpg"  data-rotate="0"  data-fstransition="no-transition" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="The Shop" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
		<!-- MAIN IMAGE -->
		<img src="<?php echo $tehAbsoluteURL; ?>layout/revTechSlider/selfDrivingCarrier.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
	
	
	<!-- SLIDE  -->
	<li data-index="rs-3046" data-transition="fadetotopfadefrombottom" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="1500"  data-thumb="<?php echo $tehAbsoluteURL; ?>layout/revTechSlider/thumbs/sterlingVARestorationsThumb.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Restorations &amp; Repairs" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
		<!-- MAIN IMAGE -->
		<img src="<?php echo $tehAbsoluteURL; ?>layout/revTechSlider/carShippingCarriers.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
		<!-- LAYERS -->

	</li>
	<!-- SLIDE  -->
	<li data-index="rs-3047" data-transition="zoomin" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="<?php echo $tehAbsoluteURL; ?>layout/revTechSlider/thumbs/sterlingVACustomsThumb.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Customs &amp; Racecars" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
		<!-- MAIN IMAGE -->
		<img src="<?php echo $tehAbsoluteURL; ?>layout/revTechSlider/mountainRoadCarrier.jpg"  alt=""  data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
		<!-- LAYERS -->

	</li>
	<!-- SLIDE  -->
	<li data-index="rs-3048" data-transition="fadetotopfadefrombottom" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="1500"  data-thumb="<?php echo $tehAbsoluteURL; ?>layout/revTechSlider/thumbs/sterlingVACarRepairsThumb.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Restorations &amp; Repairs" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
		<!-- MAIN IMAGE -->
		<img src="<?php echo $tehAbsoluteURL; ?>layout/revTechSlider/desertRoadCarrier.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
		<!-- LAYERS -->

	</li>
	
	<!-- SLIDE  -->
	<li data-index="rs-3049" data-transition="zoomin" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="<?php echo $tehAbsoluteURL; ?>layout/revTechSlider/thumbs/sterlingVArevtechShopThumb.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Contact" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
		<!-- MAIN IMAGE -->
		<img src="<?php echo $tehAbsoluteURL; ?>layout/revTechSlider/selfDrivingCarrier.jpg"  alt=""  data-bgposition="center center" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 -500" data-offsetend="0 500" data-bgparallax="10" class="rev-slidebg" data-no-retina>
		<!-- LAYERS -->

	</li>
</ul>
<div class="tp-bannertimer" style="height: 7px; background-color: rgba(255, 255, 255, 0.25);"></div>	</div>
</div><!-- END REVOLUTION SLIDER -->
		<script type="text/javascript">
			var tpj=jQuery;
			var revapi1078;
			tpj(document).ready(function() {
				if(tpj("#rev_slider_1078_2").revolution == undefined){
					revslider_showDoubleJqueryError("#rev_slider_1078_2");
				}else{
					revapi1078 = tpj("#rev_slider_1078_2").show().revolution({
						sliderType:"standard",
						jsFileLocation:"revolution/js/",
						sliderLayout:"fullwidth",
						dottedOverlay:"none",
						delay:9000,
						navigation: {
							keyboardNavigation:"off",
							keyboard_direction: "horizontal",
							mouseScrollNavigation:"off",
 							mouseScrollReverse:"default",
							onHoverStop:"off",
							touch:{
								touchenabled:"on",
								swipe_threshold: 75,
								swipe_min_touches: 1,
								swipe_direction: "horizontal",
								drag_block_vertical: false
							}
							,
							arrows: {
								style:"zeus",
								enable:false,
								hide_onmobile:true,
								hide_under:600,
								hide_onleave:true,
								hide_delay:200,
								hide_delay_mobile:1200,
								tmp:'<div class="tp-title-wrap">  	<div class="tp-arr-imgholder"></div> </div>',
								left: {
									h_align:"left",
									v_align:"center",
									h_offset:30,
									v_offset:0
								},
								right: {
									h_align:"right",
									v_align:"center",
									h_offset:30,
									v_offset:0
								}
							}
							,
							bullets: {
								enable:false,
								hide_onmobile:true,
								hide_under:600,
								style:"metis",
								hide_onleave:true,
								hide_delay:200,
								hide_delay_mobile:1200,
								direction:"horizontal",
								h_align:"center",
								v_align:"bottom",
								h_offset:0,
								v_offset:30,
								space:5,
								tmp:'<span class="tp-bullet-img-wrap">  <span class="tp-bullet-image"></span></span><span class="tp-bullet-title">{{title}}</span>'
							}
						},
						viewPort: {
							enable:true,
							outof:"pause",
							visible_area:"80%",
							presize:false
						},
						responsiveLevels:[1240,1024,778,480],
						visibilityLevels:[1240,1024,778,480],
						gridwidth:[1240,1024,778,480],
						//gridheight:[650,650,650,650],
						//gridheight:[600,600,500,400],
						gridheight:[850,850,850,850],
						lazyType:"none",
						parallax: {
							type:"mouse",
							origo:"slidercenter",
							speed:2000,
							levels:[2,3,4,5,6,7,12,16,10,50,47,48,49,50,51,55],
							type:"mouse",
						},
						shadow:0,
						spinner:"off",
						stopLoop:"off",
						stopAfterLoops:1,
						stopAtSlide:1,
						shuffle:"off",
						autoHeight:"on",
						hideThumbsOnMobile:"off",
						hideSliderAtLimit:0,
						hideCaptionAtLimit:0,
						hideAllCaptionAtLilmit:0,
						debugMode:false,
						fallbacks: {
							simplifyAll:"off",
							nextSlideOnWindowFocus:"off",
							disableFocusListener:false,
						}
					});
				}
			});	/*ready*/
		</script>
		

				</article>
		</section>
    <div class="clearfix"></div>