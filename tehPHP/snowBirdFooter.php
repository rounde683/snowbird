<style>
	.preFooterShell{padding: 50px 5%;}
	.preFooterTitle{font-size: 4em;}
	.preFooterSubTitle{font-size: 2em;}

	.snowBirdStateShippingList{list-style-type: square;}
</style>
<div class="preFooterShell snowBirdBlueBG">
	<div class="preFooterTitle centerText bold whiteText">
		Licensed and Insured
	</div>
	<div class="preFooterSubtitle centerText whiteText">
		 No risk, Low Price, Guarantee <i class="fa fa-certificate" aria-hidden="true"></i>
	</div>
</div>
<div class="snowBirdContentWrapper">
	<div class="snowBirdDivShell">
		<div class="snowBirdContentTitleShell">
			<div class="snowBirdContentTitle bold">
				Serving 48 Contiguous States
			</div>
			<div class="snowBirdContentSubTitle" style="margin-top: -10px; margin-bottom: 100px;">
				Voted #1 among SnowBirds
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="snowBirdContentColumnPaddingShell usMap">
					<center>
						<img src="<?php echo $tehAbsoluteURL; ?>layout/images/usMap.png" width="100%" />
					</center>
				</div>
			</div>
		</div>
		<br /><br /><br /><br />
		<div class="row" style="padding: 20px 40px;">
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<ul class="snowBirdStateShippingList">
					<li>
						<a href="<?php echo $tehAbsoluteURL; ?>carTransportStatetoState/">
							State to State Auto Transport
						</a>
					</li>
					<li>
						<a href="<?php echo $tehAbsoluteURL; ?>carTransportStatetoState/carTransportFLtoCA.php">
							Car Transportation FL to CA
						</a>
					</li>
					<li>
						<a href="<?php echo $tehAbsoluteURL; ?>carTransportStatetoState/carTransportNYtoFL.php">
							Car Transportation NY to FL
						</a>
					</li>
					<li>
						<a href="<?php echo $tehAbsoluteURL; ?>carTransportStatetoState/interstateCarTransporting.php">
							Interstate Car Transporting
						</a>
					</li>
				</ul>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<ul class="snowBirdStateShippingList">
					<li>
						<a href="<?php echo $tehAbsoluteURL; ?>snowbirdCarTransport/">
							Snowbird Car Transportation
						</a>
					</li>
				</ul>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<ul class="snowBirdStateShippingList">
					<li>
						<a href="<?php echo $tehAbsoluteURL; ?>enclosedAutoTransport/">
							Enclosed Auto Transport
						</a>
					</li>
				</ul>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<ul class="snowBirdStateShippingList">
					<li>
						<a href="<?php echo $tehAbsoluteURL; ?>floridaCarTransport/">
							Florida Car Transport
						</a>
					</li>
					<li>
						<a href="<?php echo $tehAbsoluteURL; ?>floridaCarTransport/carTransportMiami.php">
							Miami Auto Transport
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="snowBirdContentWrapper" style="margin-top: -100px;">
	<div class="snowBirdDivShell">
		<div class="snowBirdContentTitle bold snowBirdBlue centerText">
			Put us to work for you!
		</div>
		<div class="snowBirdContentSubTitle centerText">
			"We'll handle everything, you just tell us where you're going."
		</div>
		<br />
		<center>
			<button type="button" onfocus="blur()" class="snowBirdButtonShell" onclick="navRefrralForm()" style="background: none; border: none;">
				<a href="#tehReferralForm" id="snowBirdFreeQuoteButton" class="snowBirdButton">
					Car Transport Free Quote
				</a>
			</button>
		</center>
	</div>
</div>
<div class="footerShell">
	<div class="centerText">
		<img height="75" alt="Snowbird Hauling Logo" title="Snowbird Hauling: Hauling Cars is what we do."  src="<?php echo $tehAbsoluteURL; ?>layout/logos/snowBirdLogo.png" />
	</div>
	<div class="snowBirdBlue oneHalfText textShadowLight centerText">
		Snowbird Hauling
	</div>
	<div class="centerText bold snowBirdBlue" style="font-size: 1.5em; margin-bottom: 50px;">
		info@snowbirdhauling.com
	</div>
</div>
</body>
</html>

<script>
	function navRefrralForm()
	{
		$('html, body').animate({
        	scrollTop: $("#referralFormLogo").offset().top
    	}, 1000);
    	setTimeout(function() { $('#focusThis').focus(); }, 1300);
	}

	$(document).ready(function() {
		$(".snowBirdStickyHeader").hide();
	});
	window.onscroll = function() {myFunction()};
	var header = document.getElementById("snowBirdStickyHeaderID");
	var sticky = header.offsetTop;

	function myFunction()
	{
		if (window.pageYOffset > sticky)
		{
			header.classList.add("sticky");
			$(".snowBirdStickyHeader").show();
  		} else {
			header.classList.remove("sticky");
			$(".snowBirdStickyHeader").hide();
		}
	}
</script>