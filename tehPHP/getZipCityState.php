<?php

require_once("sqlConfig.php");

// $countries = getCountries();

$phrase = "";
$stmt = "";
$rows = "";

if(isset($_POST['phrase']))
{
	$phrase = $_POST['phrase'];
}

try {

	if(strlen($phrase) > 0)
	{
		$stmt = $dbh->prepare("SELECT ZIP_CODE,CITY,STATE_PREFIX FROM ZIP_CODE WHERE ZIP_CODE LIKE ?");
		//$stmt->bindValue(1, "%".$phrase."%", PDO::PARAM_STR);
		$stmt->bindValue(1, $phrase."%", PDO::PARAM_STR);
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
} catch(PDOException $ex) {
	echo "An Error occured!"; //user friendly message
    //some_logging_function($ex->getMessage());
}

$dataType = "json";

if(isset($_POST['dataType'])) {
	$dataType = $_POST['dataType'];
}
switch($dataType) {

	case "json":
	$json = json_encode($rows);
	header('Content-Type: application/json');
	echo $json;

	break;

	case "xml":

		$xml .= '<data>';

		foreach($found_countries as $key => $country) {
			$xml .= '<country>' . $country . '</country>';
		}

		$xml .= '</data>';


		header('Content-Type: text/xml');
		echo $xml;
	break;

	default:
	break;

}

?>