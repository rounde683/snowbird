<?php
	require_once("../sqlConfig.php");

	$originField;
	$destField;
	$cName;
	$cEmail;
	$cPhone;

	function isValidZipCode($zipCode)
	{
		return (preg_match('#[0-9]{5}#', $zipCode)) ? true : false;
	}

	if(!(isset($_POST['originatingCityZip'])))
	{
		return false;
	}
	if(!(isset($_POST['destinationCityZip'])))
	{
		return false;
	}
	if(!(isset($_POST['contactName'])))
	{
		return false;
	}
	if(!(isset($_POST['contactEmail'])))
	{
		return false;
	}
	if(!(isset($_POST['contactPhone'])))
	{
		return false;
	}

	if(!(isValidZipCode($_POST['originatingCityZip'])))
	{
		return false;
	}
	if(!(isValidZipCode($_POST['destinationCityZip'])))
	{
		return false;
	}
	if(!(filter_var($_POST['contactEmail'], FILTER_VALIDATE_EMAIL)))
	{
		return false;
	}

	$originField = $_POST['originatingCityZip'];
	$destField = $_POST['destinationCityZip'];
	$cName = $_POST['contactName'];
	$cEmail = $_POST['contactEmail'];
	$cPhone = $_POST['contactPhone'];

	try
	{
		if(strlen($originField) > 0)
		{
			$formID = md5(uniqid(rand(), true));

			$stmt = $dbh->prepare("INSERT INTO form_submissions (form_id,form_origin,form_dest,form_name,form_email,form_phone) values(?,?,?,?,?,?)");
			//$stmt->bindValue(1, "%".$phrase, PDO::PARAM_STR);
			$stmt->bindValue(1, $formID, PDO::PARAM_STR);
			$stmt->bindValue(2, $originField, PDO::PARAM_STR);
			$stmt->bindValue(3, $destField, PDO::PARAM_STR);
			$stmt->bindValue(4, $cName, PDO::PARAM_STR);
			$stmt->bindValue(5, $cEmail, PDO::PARAM_STR);
			$stmt->bindValue(6, $cPhone, PDO::PARAM_STR);
			$stmt->execute();
			$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}
	} catch(PDOException $ex) {
		return false;
		//echo "An Error occured!"; //user friendly message
		//some_logging_function($ex->getMessage());
	}

	// RETURN FAIL the jQuery Post will detect this and fail
	//return false;
	// $error = 'Always throw this error';
 	// throw new Exception($error);

	//Our POST is expecting JSON if we return with anything other than JSON, we will error
	//$formFields = array('origin' => $originField, 'dest' => $destField, 'name' => $cName, 'email' => $cEmail, 'phone' => $cPhone);
	$formFields = array('success' => "Form Submitted Successfully!");
	echo json_encode($formFields);
?>