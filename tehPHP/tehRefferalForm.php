
<script src="<?php echo $tehAbsoluteURL; ?>js/parsley.min.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo $tehAbsoluteURL; ?>js/jquery.easy-autocomplete.js" type="text/javascript" charset="utf-8"></script>

<link rel="stylesheet" type="text/css" href="<?php echo $tehAbsoluteURL; ?>css/parsley.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $tehAbsoluteURL; ?>css/easy-autocomplete.css" />

<a name="tehReferralForm"></a> 
<div class="centerText" >
	<div>
		<img id="referralFormLogo" height="75" alt="Snowbird Hauling Logo" title="Snowbird Hauling: Hauling Cars is what we do."  src="<?php echo $tehAbsoluteURL; ?>layout/logos/snowBirdLogo.png" />
	</div>
    <div class="snowBirdBlue bold" style="font-size: 1.4em; text-align: center !important;">
        Snowbird Hauling
    </div>
</div>
<div class="snowBirdFormLogoShell">
	<br style="clear: both;"/>
	<div class="contentSubTitle centerText snowBirdBlue bold">
		FREE Quote, no obligation
	</div>
	<p class="centerText" style="font-size: .8em;">
		We'll find you the best car transport pricing
	</p>
</div>
<div class="snowBirdSuccessFormMessageShell parsley-success hidden">
	<div class="bold">
		Thank you!
	</div>
	<br />
	<p>
		Your quote has been submitted <b>successfully</b>! Now we'll put one of our specialists to <b>work for you</b> and get back to you within <b>24 hours</b>.
	</p>
	<p>
		Sit back and enjoy!
	</p>
</div>

<form id="snowBirdQuoteForm" method="POST" data-parsley-validate="">
	<label for="originatingCityZip">Route</label>
	<input type="text" id="originatingCityZip" class="form-control snowBirdInput" name="originatingCityZip" required="" placeholder="Zip (Originating)" minlength="5" maxlength="44" size="5" data-parsley-trigger="change">
	<input type="text" id="destinationCityZip" class="form-control snowBirdInput" name="destinationCityZip" required="" placeholder="Zip (Destination)" minlength="5" maxlength="44" size="5" data-parsley-trigger="change">
	<label for="contactName">Contact Info</label>
	<input type="text" class="form-control snowBirdInput" name="contactName" required="" placeholder="Name" minlength="2" size="32" data-parsley-trigger="change">
	<input type="text" data-parsley-type="email" class="form-control snowBirdInput" name="contactEmail" required="" placeholder="youremail@email.com" size="32" data-parsley-trigger="change">
	<input type="text" data-parsley-type="digits" class="form-control snowBirdInput" name="contactPhone" required="" placeholder="xxxxxxxxxx" minlength="10" maxlength="10" size="10" data-parsley-trigger="change">
	<br />
	<center>
		<button type="submit" onfocus="blur()" class="snowBirdButtonShell" style="background: none; border: none;">
			<a class="snowBirdButton">
				Request Quote
			</a>
		</button>
	</center>
</form>
<script type="text/javascript">
	$(function () {
		$('#snowBirdQuoteForm').parsley().on('field:validated', function() {
		var ok = $('.parsley-error').length === 0;
	})
	.on('form:submit', function() {
  		event.preventDefault();
  		var formData = {
            'originatingCityZip'	: $('input[name=originatingCityZip]').val(),
            'destinationCityZip'	: $('input[name=destinationCityZip]').val(),
            'contactName'			: $('input[name=contactName]').val(),
            'contactEmail'			: $('input[name=contactEmail]').val(),
            'contactPhone'			: $('input[name=contactPhone]').val()
        };
  	$.ajax({
	    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
	    url         : '<?php echo $tehAbsoluteURL; ?>tehPHP/formValidations/snowbirdFreeQuoteValidation.php', // the url where we want to POST
	    data        : formData, // our data object
	    dataType    : 'json' // what type of data do we expect back from the server
	})
    // using the done promise callback
    .done(function(data) {
    	$('#snowBirdQuoteForm').toggleClass('hidden');
    	$('.snowBirdFormLogoShell').toggleClass('hidden');
    	$('.snowBirdSuccessFormMessageShell').toggleClass('hidden');
    })

    // using the fail promise callback
    .fail(function(data) {

        // show any errors
        // best to remove for production
        console.log("fail");
        //console.log(data);
        //create generic error message
    });
    return false; // Don't submit form for this demo
  });
});
</script>
	<script>
		var optionsOrig = {

		  url: function(phrase) {
		    return "<?php echo $tehAbsoluteURL; ?>tehPHP/getZipCityState.php";
		  },

		  getValue: function(element) {
		    return element.ZIP_CODE +", " + element.CITY + " " + element.STATE_PREFIX;
		  },

		  ajaxSettings: {
		    dataType: "json",
		    method: "POST",
		    data: {
		      dataType: "json"
		    }
		  },

		  preparePostData: function(data) {
		    data.phrase = $("#originatingCityZip").val();
		    return data;
		  },

		  requestDelay: 400
		};
		var optionsDest = {

		  url: function(phrase) {
		    return "<?php echo $tehAbsoluteURL; ?>tehPHP/getZipCityState.php";
		  },

		  getValue: function(element) {
		    return element.ZIP_CODE +", " + element.CITY + " " + element.STATE_PREFIX;
		  },

		  ajaxSettings: {
		    dataType: "json",
		    method: "POST",
		    data: {
		      dataType: "json"
		    }
		  },

		  preparePostData: function(data) {
		    data.phrase = $("#destinationCityZip").val();
		    return data;
		  },

		  requestDelay: 400
		};

		//$("#example-ajax-post").easyAutocomplete(options);
		$("#originatingCityZip").easyAutocomplete(optionsOrig);
		$("#destinationCityZip").easyAutocomplete(optionsDest);
	</script>
<!--
<form id="contactForm">
	<fieldset>
		<div class="contactUsFormTitleBlack">
			Originating City (Zip)
		</div>
		<div class="form-group">
			<input type="text" id="focusThis" name="originatingCityZip" size="6" class="form-control originatingCityZipDisplay" placeholder="Zip" data-vindicate="required|format:alphanumeric" />
			<small class="form-control-feedback"></small>
			<input type="hidden" name="originatingCityZip" size="6" class="form-control originatingCityZip" placeholder="Zip" data-vindicate="required|format:numeric" />
			<small class="form-control-feedback"></small>
			<input type="hidden" name="originatingCitys" size="32" class="form-control originatingCitys" placeholder="City" data-vindicate="required|format:alpha" />
			<small class="form-control-feedback"></small>
			<input type="hidden" name="originatingST" size="2" class="form-control originatingST" placeholder="ST" data-vindicate="required|format:alpha" />
			<small class="form-control-feedback"></small>
		</div>
		<div class="contactUsFormTitleBlack">
			Name
		</div>
		<div class="form-group">
			<input type="text" name="active_full_name" class="form-control" placeholder="Your full name..." data-vindicate="required|format:alpha" />
			<small class="form-control-feedback"></small>
		</div>
		<div class="contactUsFormTitleBlack">
			Destination City (Zip)
		</div>
		<div class="form-group">
			<input type="text" name="destinationCityZip" class="form-control destinationCityZipDisplay" placeholder="Zip" data-vindicate="required|format:alphanumeric" />
			<small class="form-control-feedback"></small>
			<input type="hidden" name="destinationCityZip" class="form-control destinationCityZip" placeholder="Zip" data-vindicate="required|format:numeric" />
			<small class="form-control-feedback"></small>
			<input type="hidden" name="destinationCity" class="form-control destinationCity" placeholder="City" data-vindicate="required|format:alpha" />
			<small class="form-control-feedback"></small>
			<input type="hidden" name="destinationST" class="form-control destinationST" placeholder="ST" data-vindicate="required|format:alpha" />
			<small class="form-control-feedback"></small>
		</div>
		<div class="contactUsFormTitleBlack">
			Phone
		</div>
		<div class="form-group">
			<input type="text" name="active_phone_number" class="form-control" placeholder="(xxx) xxx-xxx" data-vindicate="required|format:phone|active" />
			<small class="form-control-feedback"></small>
		</div>

		<div>
			<div class="form-group">
				<input type="text" name="active_full_name" class="form-control" placeholder="youremail@email.com" data-vindicate="required|format:email|active" />
				<small class="form-control-feedback"></small>
			</div>
			<center>
				<button type="button" onfocus="blur()" class="snowBirdButtonShell" onclick="submitBasicFeedback()" style="background: none; border: none;">
					<a class="snowBirdButton">
						Request Quote
					</a>
				</button>
			</center>
		</div>
	</fieldset>
</form>
-->