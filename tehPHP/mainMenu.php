<div class="mainMenuShell">
    <nav>
        <ul class="mainMenu">
            
        </ul>
        <div class="button" style="position: fixed; top: 5px; right: 25px; z-index: 5000;">
            <a class="btn-open" href="#"></a>
        </div>
    </nav>
    <div class="overlay">
        <div class="wrap">
            <ul class="wrap-nav">
                <li><a href="<?php echo $tehAbsoluteURL; ?>">Home</a>
                    <!--
                <ul>
                    <li><a href="<?php echo $tehAbsoluteURL; ?>aboutUs.php">About</a></li>
                    <li><a href="<?php echo $tehAbsoluteURL; ?>contactUs.php">Contact</a></li>
                </ul>
            -->
                </li>
                <li><a href="<?php echo $tehAbsoluteURL; ?>carTransportStatetoState/">Car Transport State to State</a>
                <ul>
                    <li>
                        <a href="<?php echo $tehAbsoluteURL; ?>carTransportStatetoState/carTransportNYtoFL.php">
                            Car Transport NY to Florida
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo $tehAbsoluteURL; ?>carTransportStatetoState/carTransportFLtoCA.php">
                            Car Transport from Florida to California
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo $tehAbsoluteURL; ?>carTransportationServices/crossCountryAutoTransport.php">
                            Cross Crountry Auto Transport
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo $tehAbsoluteURL; ?>carTransportStatetoState/interstateCarTransporting.php">
                            Interstate Car Transporting
                        </a>
                    </li>
                </ul>
                </li>
                <li><a href="<?php echo $tehAbsoluteURL; ?>snowbirdCarTransport/">Snowbird Car Transport</a></li>
                <li><a href="<?php echo $tehAbsoluteURL; ?>enclosedAutoTransport/">Enclosed Auto Transport</a></li>
            </ul>
            <!--
            <div class="social">
                <a href="https://www.facebook.com/innacherndds/">
                    <div class="social-icon">
                        <i class="fa fa-facebook"></i>
                    </div>
                </a>
                <a href="http://instagram.com/innacherndds">
                    <div class="social-icon">
                        <i class="fab fa-instagram"></i>
                    </div>
                </a>
                <a href="https://www.linkedin.com/in/inna-chern-5787413b">
                    <div class="social-icon">
                        <i class="fab fa-linkedin-in"></i>
                    </div>
                </a>
                <a href="mailto:innacherndds@gmail.com">
                    <div class="social-icon">
                        <i class="fas fa-envelope"></i>
                    </div>
                </a>
            </div>
            -->
        </div>
    </div>
</div>
<script src="<?php echo $tehAbsoluteURL; ?>js/overlayMenuFS.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="<?php echo $tehAbsoluteURL; ?>css/overlayMenuFS.css">