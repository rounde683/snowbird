<script>
$(document).ready(function() {

// 	var options = {

// 	  url: function(phrase) {
// 	    //return "api/countrySearch.php";
// 	    return "tehPHP/getZipCityState.php";
// 	  },

// 	  getValue: function(element) {
// 	    return element.ZIP_CODE +", " + element.CITY + " " + element.STATE_PREFIX;
// 	  },

// 	  ajaxSettings: {
// 	    dataType: "json",
// 	    method: "POST",
// 	    data: {
// 	      dataType: "json"
// 	    }
// 	},
// 		preparePostData: function(data) {
// 		//data.phrase = $("#example-ajax-post").val();
// 		data.phrase = $(".originatingCityZipDisplay").val();
// 		return data;
// 	},

// 	requestDelay: 400,

// 	list: {
// 	  	onSelectItemEvent: function(data) {
// 			//var selectedItemValue = $("#originatingCityZip").getSelectedItemData().realName;
// 			var selectedItemValue = $(".originatingCityZipDisplay").getSelectedItemData();
//             $(".originatingCityZip").val(selectedItemValue.ZIP_CODE).trigger("change");
//             $(".originatingCity").val(selectedItemValue.CITY).trigger("change");
//             $(".originatingST").val(selectedItemValue.STATE_PREFIX).trigger("change");
// 		},
// 		onHideListEvent: function() {
// 			//$("#originatingST").val("").trigger("change");
// 		}
// 	}
// 	};

// 	var destinationOptions = {

// 	  url: function(phrase) {
// 	    //return "api/countrySearch.php";
// 	    return "tehPHP/getZipCityState.php";
// 	  },

// 	  getValue: function(element) {
// 	    return element.ZIP_CODE +", " + element.CITY + " " + element.STATE_PREFIX;
// 	  },

// 	  ajaxSettings: {
// 	    dataType: "json",
// 	    method: "POST",
// 	    data: {
// 	      dataType: "json"
// 	    }
// 	},
// 		preparePostData: function(data) {
// 		//data.phrase = $("#example-ajax-post").val();
// 		data.phrase = $(".destinationCityZipDisplay").val();
// 		return data;
// 	},

// 	requestDelay: 400,

// 	list: {
// 	  	onSelectItemEvent: function(data) {
// 			//var selectedItemValue = $("#originatingCityZip").getSelectedItemData().realName;
// 			var selectedItemValue = $(".destinationCityZipDisplay").getSelectedItemData();
//             $(".destinationCityZip").val(selectedItemValue.ZIP_CODE).trigger("change");
//             $(".destinationCity").val(selectedItemValue.CITY).trigger("change");
//             $(".destinationST").val(selectedItemValue.STATE_PREFIX).trigger("change");
// 		},
// 		onHideListEvent: function() {
// 			//$("#originatingST").val("").trigger("change");
// 		}
// 	}
// };

// 	$(".originatingCityZipDisplay").easyAutocomplete(options);
// 	$(".destinationCityZipDisplay").easyAutocomplete(destinationOptions);

});
</script>
<?php
	//require_once("./tehPHP/LandingPageSlider.php");
?>


<?php
	//require_once("./tehPHP/mainRevolutionSlider.php");
?>
<div class="snowBirdContentWrapper">
	<div class="snowBirdDivShell">
		<div class="snowBirdContentTitleShell">
			<div class="snowBirdContentTitle ">
				Competitive Pricing
			</div>
			<div class="snowBirdContentSubTitle">
				Pricing varies upon season, demand, a vehicle type
			</div>
		</div>
		<br /><br /><br />
		<div class="row snowBirdDisplayTextSize snowBirdBlue">
			<div class="col-lg-10">
				New York <i class="fa fa-arrow-right" aria-hidden="true"></i> Boca Raton 
			</div>
			<div class="col-lg-2">
				$749
			</div>
		</div>
		<div class="row snowBirdDisplayTextSize snowBirdBlue">
			<div class="col-lg-10">
				LA <i class="fa fa-arrow-right" aria-hidden="true"></i> New York
			</div>
			<div class="col-lg-2">
				$1349
			</div>
		</div>
		<div class="row snowBirdDisplayTextSize snowBirdBlue">
			<div class="col-lg-10">
				Boston <i class="fa fa-arrow-right" aria-hidden="true"></i> Charleston
			</div>
			<div class="col-lg-2">
				$689
			</div>
		</div>
		<div class="row snowBirdDisplayTextSize snowBirdBlue">
			<div class="col-lg-10">
				Detroit <i class="fa fa-arrow-right" aria-hidden="true"></i> San Diego
			</div>
			<div class="col-lg-2">
				$989
			</div>
		</div>
		<div class="row snowBirdDisplayTextSize snowBirdBlue">
			<div class="col-lg-10">
				New York <i class="fa fa-arrow-right" aria-hidden="true"></i> Seattle
			</div>
			<div class="col-lg-2">
				$1129
			</div>
		</div>
		<div class="row snowBirdDisplayTextSize snowBirdBlue">
			<div class="col-lg-10">
				Portland <i class="fa fa-arrow-right" aria-hidden="true"></i> Chicago
			</div>
			<div class="col-lg-2">
				$719
			</div>
		</div>
		<div class="row snowBirdDisplayTextSize snowBirdBlue">
			<div class="col-lg-10">
				Billings <i class="fa fa-arrow-right" aria-hidden="true"></i> Phoenix
			</div>
			<div class="col-lg-2">
				$589
			</div>
		</div>
		<div class="row snowBirdDisplayTextSize snowBirdBlue">
			<div class="col-lg-10">
				Miami <i class="fa fa-arrow-right" aria-hidden="true"></i> Kansas City
			</div>
			<div class="col-lg-2">
				$639
			</div>
		</div><div class="row snowBirdDisplayTextSize snowBirdBlue">
			<div class="col-lg-10">
				Orlando <i class="fa fa-arrow-right" aria-hidden="true"></i> New Orleans
			</div>
			<div class="col-lg-2">
				$799
			</div>
		</div>
		<br />
		<div class="snowBirdContentSubTitle" style="font-size: .9em;">
			* Prices above do not necessarily reflect current demand and should be considered a rough estimate.
		</div>
	</div>
</div>
<div class="snowBirdContentWrapper">
	<div class="snowBirdDivShell">
	</div>
</div>




<div class="container contactUsFormShell" style="margin-top:15px;">
	<div class="card">
		<div class="card-header">
			<h2 class="card-title"><b>FREE Quote</b> - No Obligation</h2>
		</div>
<div class="card-block">
	<form id="contactForm">
		<fieldset>
			<div class="contactUsFormTitleBlack">
				Originating City (Zip)
			</div>
			<div class="form-group">
				<input type="text" name="originatingCityZip" size="6" class="form-control originatingCityZipDisplay" placeholder="Zip" data-vindicate="required|format:alphanumeric" />
				<small class="form-control-feedback"></small>
				<input type="hidden" name="originatingCityZip" size="6" class="form-control originatingCityZip" placeholder="Zip" data-vindicate="required|format:numeric" />
				<small class="form-control-feedback"></small>
				<input type="hidden" name="originatingCity" size="32" class="form-control originatingCity" placeholder="City" data-vindicate="required|format:alpha" />
				<small class="form-control-feedback"></small>
				<input type="hidden" name="originatingST" size="2" class="form-control originatingST" placeholder="ST" data-vindicate="required|format:alpha" />
				<small class="form-control-feedback"></small>
			</div>
			<div class="contactUsFormTitleBlack">
				Destination City (Zip)
			</div>
			<div class="form-group">
				<input type="text" name="destinationCityZip" class="form-control destinationCityZipDisplay" placeholder="Zip" data-vindicate="required|format:alphanumeric" />
				<small class="form-control-feedback"></small>
				<input type="hidden" name="destinationCityZip" class="form-control destinationCityZip" placeholder="Zip" data-vindicate="required|format:numeric" />
				<small class="form-control-feedback"></small>
				<input type="hidden" name="destinationCity" class="form-control destinationCity" placeholder="City" data-vindicate="required|format:alpha" />
				<small class="form-control-feedback"></small>
				<input type="hidden" name="destinationST" class="form-control destinationST" placeholder="ST" data-vindicate="required|format:alpha" />
				<small class="form-control-feedback"></small>
			<div class="contactUsFormTitleBlack">
				Contact Info
			</div>
			<div class="form-group">
				<input type="text" name="active_full_name" class="form-control" placeholder="Your full name..." data-vindicate="required|format:alpha" />
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<input type="text" name="active_phone_number" class="form-control" placeholder="(xxx) xxx-xxx" data-vindicate="required|format:phone|active" />
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<input type="text" name="active_full_name" class="form-control" placeholder="xxxxxxxxx@xxxxx.com" data-vindicate="required|format:email|active" />
				<small class="form-control-feedback"></small>
			</div>
			<center>
				<button type="button" onfocus="blur()" class="kulaGrowButtonShell" onclick="submitBasicFeedback()" style="background: none; border: none;">
					<a class="growButtonRedefine">
						Request Quote
					</a>
				</button>
			</center>
</fieldset>
</form>
</div>
</div>
</div>
</div>
</div>
<br /><br /><br /><br /><br />
<div class="snowBirdBlueBG">
	<div class="snowBirdContentWrapper">
		<div class="snowBirdDivShell">
			<div class="snowBirdContentTitleShell">
				<div class="snowBirdContentTitle snowBirdWhiteText">
					Competitive Pricing
				</div>
				<div class="snowBirdContentSubTitle snowBirdWhiteText">
					Pricing varies upon season, demand, a vehicle type
				</div>
			</div>
		</div>
	</div>
</div>
	
<div class="snowBirdContentWrapper">
	<div class="snowBirdDivShell">
		<div class="snowBirdContentTitleShell">
			<div class="snowBirdContentTitle">
				Serving 48 Contiguous States
			</div>
			<div class="snowBirdContentSubTitle">
				Voted #1 among SnowBirds
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="snowBirdContentColumnPaddingShell usMap">
					<center>
						<img src="./layout/images/usMap.png" width="100%" />
					</center>
				</div>
			</div>
		</div>
		<div class="row" style="padding: 20px 40px;">
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<ul class="snowBirdStateShippingList">
					<li>
						<a href="#">
							Alabama Car Shipping
						</a>
					</li>
					<li>
						<a href="#">
							Alaska Car Shipping
						</a>
					</li>
					<li>
						<a href="#">
							Arizona Car Transport
						</a>
					</li>
					<li>
						<a href="#">
							Arkansas Car Shipping
						</a>
					</li>
				</ul>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<ul class="snowBirdStateShippingList">
					<li>
						<a href="#">
							Utah Car Shipping
						</a>
					</li>
					<li>
						<a href="#">
							Virginia Car Shipping
						</a>
					</li>
					<li>
						<a href="#">
							Washington Car Transport
						</a>
					</li>
					<li>
						<a href="#">
							Washington DC Car Shipping
						</a>
					</li>
				</ul>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<ul class="snowBirdStateShippingList">
					<li>
						<a href="#">
							Alabama Car Shipping
						</a>
					</li>
					<li>
						<a href="#">
							Alaska Car Shipping
						</a>
					</li>
					<li>
						<a href="#">
							Arizona Car Transport
						</a>
					</li>
					<li>
						<a href="#">
							Arkansas Car Shipping
						</a>
					</li>
				</ul>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<ul class="snowBirdStateShippingList">
					<li>
						<a href="#">
							Utah Car Shipping
						</a>
					</li>
					<li>
						<a href="#">
							Virginia Car Shipping
						</a>
					</li>
					<li>
						<a href="#">
							Washington Car Transport
						</a>
					</li>
					<li>
						<a href="#">
							Washington DC Car Shipping
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
	</div>
</div>

<script src="<?php echo $tehAbsoluteURL; ?>js/jquery.easy-autocomplete.js" type="text/javascript" charset="utf-8"></script>
<script src="js/vindicate.js"></script>

<script>
	$("#contactForm").vindicate("init");
	
	var submitBasicFeedback = function() {
  		$("#contactForm").vindicate("validate");
	}
</script>
<?php
	require_once("tehPHP/snowBirdFooter.php");
?>