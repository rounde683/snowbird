<?php
	$pageTitle = "Snowbird - Car Shipping";
	$pageKeywords = "car shipping company, autotransport company florida";
	$pageDesc = "Snowbird Hauling - Car shipping across the US. Put us to work for you.";
	require_once("./tehPHP/snowBirdHeader.php");
	require_once("./tehPHP/LandingPageSlider.php");
?>
<div class="snowBirdContentWrapper">
	<div class="snowBirdDivShell">
		<div class="snowBirdContentTitleShell centerText bold">
			<div class="snowBirdContentTitle ">
				Competitive Pricing
			</div>
			<div class="snowBirdContentSubTitle centerText">
				Pricing varies upon season, demand, and vehicle type
			</div>
		</div>
		<br /><br /><br />
		<div class="snowBirdPricesShell">
			<div class="row snowBirdDisplayTextSize snowBirdBlue">
				<div class="col-lg-10 col-md-8 col-sm-6">
					New York <i class="fa fa-arrow-right" aria-hidden="true"></i> Boca Raton 
				</div>
				<div class="col-lg-2 col-md-4 col-sm-6">
					$749
				</div>
			</div>
			<div class="row snowBirdDisplayTextSize snowBirdBlue">
				<div class="col-lg-10 col-md-8 col-sm-6">
					LA <i class="fa fa-arrow-right" aria-hidden="true"></i> New York
				</div>
				<div class="col-lg-2 col-md-4 col-sm-6">
					$1349
				</div>
			</div>
			<div class="row snowBirdDisplayTextSize snowBirdBlue">
				<div class="col-lg-10 col-md-8 col-sm-6">
					Boston <i class="fa fa-arrow-right" aria-hidden="true"></i> Charleston
				</div>
				<div class="col-lg-2 col-md-4 col-sm-6">
					$689
				</div>
			</div>
			<div class="row snowBirdDisplayTextSize snowBirdBlue">
				<div class="col-lg-10 col-md-8 col-sm-6">
					Detroit <i class="fa fa-arrow-right" aria-hidden="true"></i> San Diego
				</div>
				<div class="col-lg-2 col-md-4 col-sm-6">
					$989
				</div>
			</div>
			<div class="row snowBirdDisplayTextSize snowBirdBlue">
				<div class="col-lg-10 col-md-8 col-sm-6">
					New York <i class="fa fa-arrow-right" aria-hidden="true"></i> Seattle
				</div>
				<div class="col-lg-2 col-md-4 col-sm-6">
					$1129
				</div>
			</div>
			<div class="row snowBirdDisplayTextSize snowBirdBlue">
				<div class="col-lg-10 col-md-8 col-sm-6">
					Portland <i class="fa fa-arrow-right" aria-hidden="true"></i> Chicago
				</div>
				<div class="col-lg-2 col-md-4 col-sm-6">
					$719
				</div>
			</div>
			<div class="row snowBirdDisplayTextSize snowBirdBlue">
				<div class="col-lg-10 col-md-8 col-sm-6">
					Billings <i class="fa fa-arrow-right" aria-hidden="true"></i> Phoenix
				</div>
				<div class="col-lg-2 col-md-4 col-sm-6">
					$589
				</div>
			</div>
			<div class="row snowBirdDisplayTextSize snowBirdBlue">
				<div class="col-lg-10 col-md-8 col-sm-6">
					Miami <i class="fa fa-arrow-right" aria-hidden="true"></i> Kansas City
				</div>
				<div class="col-lg-2 col-md-4 col-sm-6">
					$639
				</div>
			</div><div class="row snowBirdDisplayTextSize snowBirdBlue">
				<div class="col-lg-10 col-md-8 col-sm-6">
					Orlando <i class="fa fa-arrow-right" aria-hidden="true"></i> New Orleans
				</div>
				<div class="col-lg-2 col-md-4 col-sm-6">
					$799
				</div>
			</div>
		</div>
		<br />
		<div class="snowBirdContentSubTitle centerText" style="font-size: .9em;">
			* Prices above do not necessarily reflect current demand and should be considered a rough estimate.
		</div>
	</div>
</div>

<?php
	if( (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1") || (dirname($_SERVER['PHP_SELF']) == "/"))
	{
		require_once("./tehPHP/snowBirdFooter.php");
	}
	else
	{
		require_once("../tehPHP/snowBirdFooter.php");
	}
?>