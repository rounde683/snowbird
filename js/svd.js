<link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.3/jquery-ui.js"></script>

<form id="profileForm" method="post" class="form-horizontal">
    <div class="form-group">
        <label class="col-xs-3 control-label">Date of birth</label>
        <div class="col-xs-5">
            <input type="text" class="form-control" name="dob" />
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-9 col-xs-offset-3">
            <button type="submit" class="btn btn-default">Validate</button>
        </div>
    </div>
</form>

<script>
$(document).ready(function() {
    $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                dob: {
                    validators: {
                        notEmpty: {
                            message: 'The date of birth is required'
                        },
                        date: {
                            format: 'MM/DD/YYYY',
                            message: 'The date of birth is not valid'
                        }
                    }
                }
            }
        })
        .find('[name="dob"]')
            .datepicker({
                onSelect: function(date, inst) {
                    /* Revalidate the field when choosing it from the datepicker */
                    $('#profileForm').formValidation('revalidateField', 'dob');
                }
            });
});
</script>